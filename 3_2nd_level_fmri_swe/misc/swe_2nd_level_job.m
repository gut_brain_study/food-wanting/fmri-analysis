%-----------------------------------------------------------------------
% Job saved on 22-Mar-2021 16:33:01 by cfg_util (rev $Rev: 6942 $)
% spm SPM - SPM12 (7219)
% cfg_basicio BasicIO - Unknown
% swe SwE - Unknown
%-----------------------------------------------------------------------
addpath('/data/pt_02020/MRI_data/software/spm12/toolbox/SwE-toolbox-2.2.2')  
%('/data/pt_02161/Analysis/Software/spm12/toolbox/SwE-toolbox-2.2.2')
addpath('/data/pt_02020/MRI_data/software/spm12/')  

%%% initialise SWE
spm('defaults', 'FMRI');

% % input dir for nii files
dataDir = '/data/pt_02020/MRI_data/fmri_wanting_results/1st_level/';

% output dir
outdir = '/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/';
% 
% 
% %subjList = [01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61];
% subjList = [01 02];
% %sesList = [01 02 03 04];
% 
% subj_vector = repelem(subjList, 4)
% subj_vector = subj_vector'; %transpose to x-by-1 array (1 column only)
% 

% to do: loop over models (A / B1 / B2 / C1 / C2)
model_pattern = "modelA/"

% to do: loop over contrasts %% con_1 bis con_05
con_nii = 'con_0001'
scans_pattern = "sub-*/ses-0*/con_0001.nii"


%% create model- and contrast-specific outdir
mkdir(fullfile(outdir, model_pattern, con_nii));

%% output results .mat
output_mat = fullfile(outdir, model_pattern, con_nii, 'SwE.mat');

%% define inputdir for all con*.nii inputs
m = dir(fullfile(dataDir, model_pattern, scans_pattern));
% concatenate folder + name
scans = cell(size(m, 1), 1);
sub_ses = cell(size(m, 1), 1);
for i = 1:size(m, 1)
    scans{i} = [m(i).folder,'/',m(i).name];
    sub_ses{i} = m(i).folder(end-12:end); % strsplit(m(i).folder, filesep)
end

%check which inputs are taken
scans
sub_ses


%% load input 2nd level DM
DM = readtable('/data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/3_2nd_level_fmri_swe/Design_Matrix.csv');

%% match DM for available con.nii inputs files
DM.sub_ses = strcat(DM.subj, filesep , DM.session);
DM = DM(ismember(DM.sub_ses, sub_ses),:);


%% to do: temp: select only first two subjects
DM = DM(1:8,:);

%% create subject vector
subj_vector = DM(:,[9]);
subj_vector = subj_vector{:,1}

%% create visit vector
visits = DM(:,[14]);
visits = visits{:,1}

%% create group vector
group_vector = ones(1,length(scans));
group_vector = group_vector'; %transpose to x-by-1 array (1 column only)

%% covariate vectors
A_BL = DM.spm_A_BL;
A_FU = DM.spm_A_FU;
B_BL = DM.spm_B_BL;
B_FU = DM.spm_B_FU;

%% define contrast matrix
CM_main_effect = [0.25 0.25 0.25 0.25];
CM_intervention_effect = [-1 1 1 -1];


%% to do: loop over contrasts


%%% SwE specifics
matlabbatch{1}.spm.tools.swe.smodel.dir = cellstr(fullfile(outdir, model_pattern, con_nii));
matlabbatch{1}.spm.tools.swe.smodel.scans = cellstr(scans);
                                         
matlabbatch{1}.spm.tools.swe.smodel.ciftiAdditionalInfo.ciftiGeomFile = struct('brainStructureLabel', {}, 'geomFile', {}, 'areaFile', {});
matlabbatch{1}.spm.tools.swe.smodel.ciftiAdditionalInfo.volRoiConstraint = 1;
matlabbatch{1}.spm.tools.swe.smodel.giftiAdditionalInfo.areaFileForGiftiInputs = {};
                                         
matlabbatch{1}.spm.tools.swe.smodel.type.modified.groups = [group_vector]; % must be equal to no. of scans -> just add 1s
matlabbatch{1}.spm.tools.swe.smodel.type.modified.visits = [visits]; %% 1 2 3 4
matlabbatch{1}.spm.tools.swe.smodel.type.modified.ss = 4; %% small sample adjustment type C2
matlabbatch{1}.spm.tools.swe.smodel.type.modified.dof_mo = 3; %% approx II
matlabbatch{1}.spm.tools.swe.smodel.subjects = [subj_vector];  %% subjects / length of scans
matlabbatch{1}.spm.tools.swe.smodel.cov(1).c = [A_BL]; %% A_BL
matlabbatch{1}.spm.tools.swe.smodel.cov(1).cname = 'A_BL';
matlabbatch{1}.spm.tools.swe.smodel.cov(2).c = [A_FU]; %% A_FU
matlabbatch{1}.spm.tools.swe.smodel.cov(2).cname = 'A_FU'; 
matlabbatch{1}.spm.tools.swe.smodel.cov(3).c = [B_BL]; %% B_BL
matlabbatch{1}.spm.tools.swe.smodel.cov(3).cname = 'B_BL';
matlabbatch{1}.spm.tools.swe.smodel.cov(4).c = [B_FU]; %% B_FU
matlabbatch{1}.spm.tools.swe.smodel.cov(4).cname = 'B_FU';
                                            
matlabbatch{1}.spm.tools.swe.smodel.multi_cov.files = struct('files', {});
matlabbatch{1}.spm.tools.swe.smodel.masking.tm.tm_none = 1;
matlabbatch{1}.spm.tools.swe.smodel.masking.im = 1; % implicit mask YES
matlabbatch{1}.spm.tools.swe.smodel.masking.em = {'/data/pt_02020/MRI_data/software/ROI_analysis/neurosynth/reward_hypothalamus_merged.nii,1'}; %% explicit mask for RN + hypo
matlabbatch{1}.spm.tools.swe.smodel.WB.WB_yes.WB_ss = 4; %% to do: set to 999 non-parametric bootstrapping 
matlabbatch{1}.spm.tools.swe.smodel.WB.WB_yes.WB_nB = 10;
matlabbatch{1}.spm.tools.swe.smodel.WB.WB_yes.WB_SwE = 0;

matlabbatch{1}.spm.tools.swe.smodel.WB.WB_yes.WB_stat.WB_T.WB_T_con = [0.25 0.25 0.25 0.25]; %[CM_main_effect]; %%% define Contrast Matrix
matlabbatch{1}.spm.tools.swe.smodel.WB.WB_yes.WB_infType.WB_TFCE.WB_TFCE_E = 0.5; %% TFCE seetings
matlabbatch{1}.spm.tools.swe.smodel.WB.WB_yes.WB_infType.WB_TFCE.WB_TFCE_H = 2; %% TFCE settings
matlabbatch{1}.spm.tools.swe.smodel.globalc.g_omit = 1;
matlabbatch{1}.spm.tools.swe.smodel.globalm.gmsca.gmsca_no = 1;
matlabbatch{1}.spm.tools.swe.smodel.globalm.glonorm = 1;

% % %% specify model
matlabbatch{2}.spm.tools.swe.rmodel.des = cellstr(output_mat);
%matlabbatch{2}.spm.tools.swe.rmodel.des(1) = cfg_dep('Specify Model: SwE.mat file', substruct('.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','files'));

% %% display model results
matlabbatch{3}.spm.tools.swe.results.dis = cellstr(output_mat);
%matlabbatch{3}.spm.tools.swe.results.dis(1) = cfg_dep('Specify Model: SwE.mat file', substruct('.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','files'));

%%
% %%% initialise SWE to run script from here
class(matlabbatch)
spm('defaults', 'FMRI');
spm_jobman('run', matlabbatch)