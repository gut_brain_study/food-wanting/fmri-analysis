% List of open inputs
nrun = 1; % enter the number of runs here
jobfile = {'/data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/3_2nd_level_fmri_swe/swe_2nd_level_job.m'};
jobs = repmat(jobfile, 1, nrun);
inputs = cell(0, nrun);
for crun = 1:nrun
end

spm('defaults', 'FMRI');
swe_jobman('run', jobs, inputs{:});
