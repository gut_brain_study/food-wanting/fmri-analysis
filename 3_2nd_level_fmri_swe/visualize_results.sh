### remember in fsleyes to set min to log10(0.05) -> 1.3

### comparing results from 3 masks
fsleyes /afs/cbs.mpg.de/software/fsl/5.0.11/ubuntu-bionic-amd64/data/standard/MNI152_T1_0.5mm.nii.gz /data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/nomask/main/modelA/con_0001/swe_tfce_lpFWE-WB_c01.nii /data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/neurosynthraw/main/modelA/con_0001/swe_tfce_lpFWE-WB_c01.nii /data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/neurosynthpeak/main/modelA/con_0001/swe_tfce_lpFWE-WB_c01.nii 

#### main effect F vs. NF
fsleyes /afs/cbs.mpg.de/software/fsl/5.0.11/ubuntu-bionic-amd64/data/standard/MNI152_T1_0.5mm.nii.gz /data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/neurosynthraw/main/modelA/con_0001/swe_tfce_lpFWE-WB_c01.nii /data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/neurosynthraw/main/modelA/con_0002/swe_tfce_lpFWE-WB_c01.nii

### interaction effcet
fsleyes /afs/cbs.mpg.de/software/fsl/5.0.11/ubuntu-bionic-amd64/data/standard/MNI152_T1_0.5mm.nii.gz /data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/neurosynthraw/interaction/modelB1/con_0001/swe_tfce_lpFWE-WB_c01.nii


/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/neurosynthraw/interaction/modelB1/con_0001/swe_tfce_lpFWE-WB_c01.nii
