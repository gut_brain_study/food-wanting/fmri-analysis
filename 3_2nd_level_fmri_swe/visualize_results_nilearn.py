from nilearn import plotting
stat_img="/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/neurosynthraw/interaction/modelB1/con_0001/swe_tfce_lpFWE-WB_c01.nii"
plotting.plot_glass_brain(stat_img, output_file="/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/neurosynthraw/interaction/modelB1/swe_tfce_lpFWE-WB_c01.niiFWE0.05.pdf", display_mode='ortho', colorbar=True, figure=None, axes=None, title=None, threshold='auto', annotate=True, black_bg=False, cmap='plasma', alpha=0.7, vmin=None, vmax=None, plot_abs=True, symmetric_cbar='auto', resampling_interpolation='continuous')

