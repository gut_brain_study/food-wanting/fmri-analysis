#!/bin/bash
##author: chrschneider
###this bash script creates confound txt files for motion using fsl_motion_outliers. Before Running start FSL with: FSL --version 6.0.1

subj_list="/data/pt_02020/MRI_data/scripts/subjects_incl_ids_completed.txt"
#subj_list="/data/pt_02020/MRI_data/scripts/wd_subjects_for_BIDS.txt"
missing_subj_list="/data/pt_02020/MRI_data/scripts/missing_MRI_datapoints.txt"
in_dir="/data/pt_02020/MRI_data/BIDS/"
out_dir="/data/pt_02020/MRI_data/fmriprep/fmriprep/"

#iteration over subj_list
sed 1d $subj_list | while read line; do
  set -- $line

  subject=$(echo $2) 

#pilot participants (will not be used)
  if [[ $subject == pilot* ]]; then 
    continue 
  fi
  
  subject=$(echo $subject | grep -Eo '[0-9]{1,2}')
  subj="sub-"$subject
  
  #iteration over tp
  for j in {01..04}; do #all 4 tp's
    tp="ses-"$j

    #if subj tp task-memory in missing list -> skip
    if ! grep -P -q $subj"\ssess-"$j"\stask-memory" "$missing_subj_list"; then
      echo "Running fsl_motion_outliers on:" $subj $tp " task-memory" 
      fsl_motion_outliers -i $in_dir${subj}/${tp}/func/${subj}_${tp}_task-memory_bold.nii -o $out_dir${subj}/${tp}/func/${subj}_${tp}_task-memory_movement-confounds.tsv --fd --thresh=0.9
    else
      echo "Missing task-memory according to list:" $subj $tp
    fi

    #if subj tp task-wanting in missing list -> skip
    if ! grep -P -q $subj"\ssess-"$j"\stask-wanting" "$missing_subj_list"; then
      echo "Running fsl_motion_outliers on:" $subj $tp " task-wanting"
      fsl_motion_outliers -i $in_dir${subj}/${tp}/func/${subj}_${tp}_task-wanting_bold.nii -o $out_dir${subj}/${tp}/func/${subj}_${tp}_task-wanting_movement-confounds.tsv --fd --thresh=0.9
    else
      echo "Missing task-wanting according to list:" $subj $tp
    fi

  done
done
