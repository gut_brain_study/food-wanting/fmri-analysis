### medawar 2021-06-09
## aim: extract raw fMRI z-values from 1st level for all subjects for plot and for network analysis with microbiome data
## prereq: FSL

rm /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/*sub_list.txt 


### DATA CHECK ######
### check 1st level SPM mask.nii for completeness / holes ----
slicesdir /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/*/*/*/mask.nii
## check output html file
firefox /data/hu_medawar/slicesdir/index.html

#### OLD #### repair single datapoint 1st level SPM mask:
# fslmaths /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelA/sub-21/ses-01/mask.nii -fillh -kernel sphere 5 /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelA/sub-21/ses-01/mask.nii -odt char
# fslmaths /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelA/sub-21/ses-02/mask.nii -fillh -kernel sphere 5 /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelA/sub-21/ses-02/mask.nii -odt char
# fslmaths /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelA/sub-21/ses-03/mask.nii -fillh -kernel sphere 5 /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelA/sub-21/ses-03/mask.nii -odt char
# fslmaths /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelA/sub-21/ses-04/mask.nii -fillh -kernel sphere 5 /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelA/sub-21/ses-04/mask.nii -odt char
# fslmaths /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelA/sub-47/ses-02/mask.nii -fillh -kernel sphere 5 /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelA/sub-47/ses-02/mask.nii -odt char
# fslmaths /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelA/sub-47/ses-03/mask.nii -fillh -kernel sphere 5 /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelA/sub-47/ses-03/mask.nii -odt char
# fslmaths /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelA/sub-47/ses-04/mask.nii -fillh -kernel sphere 5 /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelA/sub-47/ses-04/mask.nii -odt char
# gzip -d -f /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/*/*/*/mask.nii.gz


###### Interaction effects #######
#####
##0. stack all images used in analysis and get a corresponding textfile
#####
fslmerge -t /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_con_01_all.nii /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelB1/*/*/con_0001.nii
printf '%s\n' /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelB1/*/*/con_0001.nii >> /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_sub_list.txt 

fslmerge -t /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0002_con_02_all.nii /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelB1/*/*/con_0002.nii
printf '%s\n' /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelB1/*/*/con_0002.nii >> /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0002_deact_sub_list.txt 

fslmerge -t /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelC2_con_0002_con_01_all.nii /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelC2/*/*/con_0002.nii
printf '%s\n' /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelC2/*/*/con_0002.nii >> /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelC2_con_0002_sub_list.txt 

#####
##1a. extract binarized mask of interaction clusters in model B1 (based on TFCE-pFWE 0.05), threshold corresponds to p = 0.05 / using fslroi to get one map per cluster #####
#####
fslmaths /data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/no_confound/allsubs/neurosynthraw/interaction/modelB1/con_0001/swe_tfce_lpFWE-WB_c01.nii -thr 1.3 -bin /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_swe_tfce_lpFWE-WB_c01_bin.nii
## check volume of mask (135 voxels)
fslstats /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_swe_tfce_lpFWE-WB_c01_bin.nii -V

## extract clusters
cluster -i /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_swe_tfce_lpFWE-WB_c01_bin.nii -t 1 -o cluster_index --osize=cluster_size

### Cluster Index	Voxels	MAX	MAX X (vox)	MAX Y (vox)	MAX Z (vox)	COG X (vox)	COG Y (vox)	COG Z (vox)
# rOFC ### 3	51	1	32	78	26	31.5	80.9	27.2
# VTA  ### 2	43	1	48	50	27	46	52.9	28.4
# rmOFC ## 1	41	1	41	85	25	40.8	83.6	27

fslmaths -dt int cluster_index -thr 3 -uthr 3 -bin /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_rOFC.nii #cluster_mask_rOFC
fslmaths -dt int cluster_index -thr 2 -uthr 2 -bin /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_VTA.nii #cluster_mask_rOFC
fslmaths -dt int cluster_index -thr 1 -uthr 1 -bin /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_rmOFC.nii #cluster_mask_rOFC


#### 1b. extract binarized DEACTIVATION cluster in model B1 con_0002 
fslmaths /data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/no_confound/allsubs/neurosynthraw/interaction/modelB1/con_0002/swe_tfce_lpFWE-WB_c02.nii -thr 1.3 -bin /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0002_swe_tfce_lpFWE-WB_c02_bin.nii
## check volume of mask (10 voxels VTA)
fslstats /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0002_swe_tfce_lpFWE-WB_c02_bin.nii -V

cluster -i /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0002_swe_tfce_lpFWE-WB_c02_bin.nii -t 1 -o cluster_index --osize=cluster_size
## Cluster Index	Voxels	MAX	MAX X (vox)	MAX Y (vox)	MAX Z (vox)	COG X (vox)	COG Y (vox)	COG Z (vox)
## VTA #### 1	10	1	44	52	28	45.3	52.8	28.4

fslmaths -dt int cluster_index -thr 1 -uthr 1 -bin /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0002_cluster_VTA_deact.nii


#### 1c. extract binarized ACTIVATION cluster in model C2 con_0002 
fslmaths /data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/no_confound/allsubs/neurosynthraw/interaction/modelC2/con_0002/swe_tfce_lpFWE-WB_c01.nii -thr 1.3 -bin /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelC2_con_0002_swe_tfce_lpFWE-WB_c01_bin.nii
## check volume of mask (3 voxels NAc)
fslstats /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelC2_con_0002_swe_tfce_lpFWE-WB_c01_bin.nii -V

cluster -i /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelC2_con_0002_swe_tfce_lpFWE-WB_c01_bin.nii -t 1 -o cluster_index --osize=cluster_size
## Cluster Index	Voxels	MAX	MAX X (vox)	MAX Y (vox)	MAX Z (vox)	COG X (vox)	COG Y (vox)	COG Z (vox)
## NAc 1	           3	1	41	         71	         32	         41	         71.3	           32.7

fslmaths -dt int cluster_index -thr 1 -uthr 1 -bin /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelC2_con_0002_cluster_NAc.nii






######
# #### start: MANUAL SPLITTING OF ROIS #########
# ### split one mask into two for two clusters
# ## Usage: fslroi <input> <output> <xmin> <xsize> <ymin> <ysize> <zmin> <zsize>
# ## Note that the arguments are minimum index and size (not maximum index). So to extract voxels 10 to 12 inclusive you would specify 10 and 3 (not 10 and 12).
# fslroi /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_swe_tfce_lpFWE-WB_c01_bin.nii /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_VTA.nii 0 -1 47 9 0 -1 0 -1
# fslroi /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_swe_tfce_lpFWE-WB_c01_bin.nii /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_rOFC.nii 26 10 0 -1 0 -1 0 -1
# fslroi /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_swe_tfce_lpFWE-WB_c01_bin.nii /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_rmOFC.nii 37 7 80 -1 0 -1 0 -1
# 
# 
# ## check volume of masks
# fslstats /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_VTA.nii -V #### 43 voxels  
# fslstats /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_rOFC.nii -V ### 51 voxels
# fslstats /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_rmOFC.nii -V ### 41 voxels
# 
# ### fill up masks with missing x/y-values (because has been cut at certain x/y-value in step before) #####
# ## create empty mask with same dimensions as cluster-mask
# fslhd -x /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_swe_tfce_lpFWE-WB_c01_bin.nii > /data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/4_extract_fmri/empty_mask.xml
# fslcreatehd /data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/4_extract_fmri/empty_mask.xml /data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/4_extract_fmri/empty_mask.nii.gz
# 
# ## for VTA (concatenante in y)
# fslroi /data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/4_extract_fmri/empty_mask.nii.gz /data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/4_extract_fmri/cropped_mask_VTA.nii 0 -1 0 47 0 -1 0 -1
# ### TO DO: fill up after 9 voxels
# fslmerge -y /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_VTA_full.nii /data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/4_extract_fmri/cropped_mask_VTA.nii /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_VTA.nii 
# 
# ## for rOFC (concatenante in x)
# fslroi /data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/4_extract_fmri/empty_mask.nii.gz /data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/4_extract_fmri/cropped_mask_rOFC.nii 0 26 0 -1 0 -1 0 -1
# fslroi /data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/4_extract_fmri/empty_mask.nii.gz /data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/4_extract_fmri/cropped_mask2_rOFC.nii 26 36 0 -1 0 -1 0 -1
# fslmerge -x /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_rOFC_full.nii /data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/4_extract_fmri/cropped_mask_rOFC.nii /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_rOFC.nii /data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/4_extract_fmri/cropped_mask2_rOFC.nii
# 
# ## for rmOFC (concatenante in x and y) #######!!!!!!! MASK 3 NOT WORKING YET!!!!!!!!
# fslroi /data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/4_extract_fmri/empty_mask.nii.gz /data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/4_extract_fmri/cropped_mask1_rmOFC.nii 0 37 0 -1 0 -1 0 -1
# fslroi /data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/4_extract_fmri/empty_mask.nii.gz /data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/4_extract_fmri/cropped_mask2_rmOFC.nii 37 44 0 -1 0 -1 0 -1
# fslroi /data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/4_extract_fmri/empty_mask.nii.gz /data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/4_extract_fmri/cropped_mask3_rmOFC.nii 0 37 0 80 0 -1 0 -1
# fslmerge -x /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_rmOFC_temp.nii /data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/4_extract_fmri/cropped_mask1_rmOFC.nii /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_rmOFC.nii /data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/4_extract_fmri/cropped_mask2_rmOFC.nii 
# 
# fslmerge -y /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_rmOFC_full.nii /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_rmOFC_temp.nii /data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/4_extract_fmri/cropped_mask3_rmOFC.nii
# 
# ## check volume of masks final
# fslstats /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_VTA_full.nii -V ## 43 voxels
# fslstats /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_rOFC_full.nii -V ## 51 voxels
# fslstats /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_rmOFC_full.nii -V
# ###
##### END OF MANUAL SPLITTING #####
####

#####
##2. extract fslstats (cluster average) from these clusters from con_0001.nii in first level of model B1
#####
fslmeants -i /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_con_01_all.nii -m /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_rOFC.nii -o /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_output_rOFC.txt
fslmeants -i /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_con_01_all.nii -m /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_VTA.nii -o /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_output_VTA.txt
fslmeants -i /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_con_01_all.nii -m /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_rmOFC.nii -o /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_output_rmOFC.txt
fslmeants -i /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0002_con_02_all.nii -m /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0002_cluster_VTA_deact.nii -o /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0002_output_VTA_deact.txt
### for model C2 con_0002
fslmeants -i /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelC2_con_0002_con_01_all.nii -m /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelC2_con_0002_cluster_NAc.nii -o /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelC2_con_0002_output_NAc.txt


#####
##3. plot mean values across all subjects --> in R 
#####
##4. give var to UFZ --> send via owncloud
#####



###### Main effects #############
##1. extract binarized mask of peak activity in primary mask (based on TFCE-pFWE 0.05), threshold corresponds to p = 0.05 #####
#####
fslmaths /data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/no_confound/allsubs/neurosynthraw/main/modelA/con_0001/swe_tfce_lpFWE-WB_c01.nii -thr 1.3 -bin /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0001_swe_tfce_lpFWE-WB_c01_bin.nii
## check volume of mask (5174 voxels)
fslstats /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0001_swe_tfce_lpFWE-WB_c01_bin.nii -V

fslmaths /data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/no_confound/allsubs/neurosynthraw/main/modelA/con_0003/swe_tfce_lpFWE-WB_c01.nii -thr 1.3 -bin /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0003_swe_tfce_lpFWE-WB_c01_bin.nii
## check volume of mask (4696 voxels)
fslstats /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0003_swe_tfce_lpFWE-WB_c01_bin.nii -V

fslmaths /data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/no_confound/allsubs/neurosynthraw/main/modelA/con_0005/swe_tfce_lpFWE-WB_c01.nii -thr 1.3 -bin /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0005_swe_tfce_lpFWE-WB_c01_bin.nii
## check volume of mask (8045 voxels)
fslstats /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0005_swe_tfce_lpFWE-WB_c01_bin.nii -V


#####
##0. stack all images used in analysis and get a corresponding textfile
#####
fslmerge -t /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0001_con_01_all.nii /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelA/*/*/con_0001.nii
fslmerge -t /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0003_con_01_all.nii /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelA/*/*/con_0003.nii
fslmerge -t /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0005_con_01_all.nii /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelA/*/*/con_0005.nii

printf '%s\n' /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelA/*/*/con_0001.nii >> /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0001_sub_list.txt 
printf '%s\n' /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelA/*/*/con_0003.nii >> /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0003_sub_list.txt 
printf '%s\n' /data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelA/*/*/con_0005.nii >> /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0005_sub_list.txt 

#####
#####
##2. extract fslstats (cluster average) from these clusters from con_000*.nii in first level of model A only (con 01 F > NF , con03 for food slope > NF slope wanting und con05 param all wanting) 
#####
fslmeants -i /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0001_con_01_all.nii -m /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0001_swe_tfce_lpFWE-WB_c01_bin.nii -o /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0001_output.txt
fslmeants -i /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0003_con_01_all.nii -m /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0003_swe_tfce_lpFWE-WB_c01_bin.nii -o /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0003_output.txt
fslmeants -i /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0005_con_01_all.nii -m /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0005_swe_tfce_lpFWE-WB_c01_bin.nii -o /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0005_output.txt

#####
##3. plot mean values across all subjects in correlation of 1 and 3; if high corr use one of them, if medium correlation, use all vars  --> in R
#####
##4. give vars to UFZ --> send via owncloud
#####



####### for neurosynth reward_hypo peak cluster -- (secondary mask) ########
## NAc, VTA , lOFC, middle frontal, frontal pole, ACC
## /data/pt_02020/MRI_data/software/ROI_analysis/neurosynth_peak/ROI_x17_sphere1.nii.gz ### NAC
## /data/pt_02020/MRI_data/software/ROI_analysis/neurosynth_peak/ROI_x17_sphere2.nii.gz  ## NAc
## /data/pt_02020/MRI_data/software/ROI_analysis/neurosynth_peak/ROI_x16_sphere1.nii.gz  ## VTA
## /data/pt_02020/MRI_data/software/ROI_analysis/neurosynth_peak/ROI_x15_sphere1.nii.gz  ## OFC
## /data/pt_02020/MRI_data/software/ROI_analysis/neurosynth_peak/ROI_x14_sphere1.nii.gz  ## mPFC
## /data/pt_02020/MRI_data/software/ROI_analysis/neurosynth_peak/ROI_x13_sphere1.nii.gz ## frontal pole
## /data/pt_02020/MRI_data/software/ROI_analysis/neurosynth_peak/ROI_x2_sphere1.nii.gz ## ACC

#####
## extract fslstats (average of all voxels of main effect) from con_0003.nii in first level of model A only (for food slope > NF slope wanting)
#####
fslmeants -i /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0003_con_01_all.nii -m /data/pt_02020/MRI_data/software/ROI_analysis/neurosynth_peak/ROI_x17_sphere1.nii.gz -o /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0003_sphere_NAc1.txt

fslmeants -i /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0003_con_01_all.nii -m /data/pt_02020/MRI_data/software/ROI_analysis/neurosynth_peak/ROI_x17_sphere2.nii.gz -o /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0003_sphere_NAc2.txt

fslmeants -i /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0003_con_01_all.nii -m /data/pt_02020/MRI_data/software/ROI_analysis/neurosynth_peak/ROI_x16_sphere1.nii.gz -o /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0003_sphere_VTA.txt

fslmeants -i /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0003_con_01_all.nii -m /data/pt_02020/MRI_data/software/ROI_analysis/neurosynth_peak/ROI_x15_sphere1.nii.gz -o /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0003_sphere_OFC.txt

fslmeants -i /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0003_con_01_all.nii -m /data/pt_02020/MRI_data/software/ROI_analysis/neurosynth_peak/ROI_x14_sphere1.nii.gz -o /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0003_sphere_mPFC.txt

fslmeants -i /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0003_con_01_all.nii -m /data/pt_02020/MRI_data/software/ROI_analysis/neurosynth_peak/ROI_x13_sphere1.nii.gz -o /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0003_sphere_frontalpole.txt

fslmeants -i /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0003_con_01_all.nii -m /data/pt_02020/MRI_data/software/ROI_analysis/neurosynth_peak/ROI_x2_sphere1.nii.gz -o /data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/main_modelA_con_0003_sphere_ACC.txt
