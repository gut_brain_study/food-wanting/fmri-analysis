#!/bin/bash
# @authors: medawar@cbs.mpg.de; thieleking@cbs.mpg.de

#fmriprep-docker /data/pt_02020/MRI_data/BIDS/ /data/pt_02020/MRI_data/fmriprep/ participant 01

# 

subj_list="/data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/0_preprocessing_using_fMRIprep/subject_to_be_processed.txt"
s
#apparantly fmriprep doesn't like to process all participants at once so iterate through each participant:
sed 1d $subj_list | while read line 
do
set -- $line
subj=$(echo $1)
id=$(echo $2) 

if [[ ! $id == pilot* ]]
then
id_nr=$(echo $id | grep -Eo '[0-9]{1,2}')
elif [[ $id == pilot* ]]
then 
continue 
fi  


#singularity run -B /data/pt_02020/MRI_data/,/afs/cbs.mpg.de/software/freesurfer/ /data/pt_02020/MRI_data/software/fmriprep-1.2.5.simg /data/pt_02020/MRI_data/BIDS /data/pt_02020/MRI_data/fmriprep/ --template MNI152NLin2009cAsym --template-resampling-grid /data/pt_02020/MRI_data/software/MNI152_T1_2mm_brain.nii.gz --fs-license-file /afs/cbs.mpg.de/software/freesurfer/licensekeys -w /data/pt_02020/MRI_data/fmriprep/wd participant --skip_bids_validation --ignore slicetiming --participant-label $id_nr

#corrected path directories Daria Jensen 20/10/2023:
singularity run -B /data/pt_02020/Data/,/afs/cbs.mpg.de/software/freesurfer/ /data/pt_02020/MRI_data/software/fmriprep-1.2.5.simg /data/pt_02020/Data/00_raw/MRI/BIDS /data/pt_02020/Data/01_preprocessing_step-1/fmriprep/fmriprep/ --template MNI152NLin2009cAsym --template-resampling-grid /data/pt_02020/Software/MNI152_T1_2mm_brain.nii.gz --fs-license-file /afs/cbs.mpg.de/software/freesurfer/licensekeys -w /data/pt_02020/Data/fmriprep/fmriprep participant --skip_bids_validation --ignore slicetiming --participant-label $id_nr

done

#### for re-run of single subj / ses
# singularity run -B /data/pt_02020/MRI_data/,/afs/cbs.mpg.de/software/freesurfer/ /data/pt_02020/MRI_data/software/fmriprep-1.2.5.simg /data/pt_02020/MRI_data/BIDS /data/pt_02020/MRI_data/fmriprep/ --template MNI152NLin2009cAsym --template-resampling-grid /data/pt_02020/MRI_data/software/MNI152_T1_2mm_brain.nii.gz --fs-license-file /afs/cbs.mpg.de/software/freesurfer/licensekeys -w /data/pt_02020/MRI_data/fmriprep/wd participant --skip_bids_validation --ignore slicetiming --participant-label 15


#### test with ICA AROMA
# singularity run -B /data/pt_02020/MRI_data/,/afs/cbs.mpg.de/software/freesurfer/ /data/pt_02020/MRI_data/software/fmriprep-1.2.5.simg /data/pt_02020/MRI_data/BIDS /data/pt_02020/MRI_data/fmriprep/fmriprep/ica_aroma --template MNI152NLin2009cAsym --template-resampling-grid /data/pt_02020/MRI_data/software/MNI152_T1_2mm_brain.nii.gz --fs-license-file /afs/cbs.mpg.de/software/freesurfer/licensekeys -w /data/pt_02020/MRI_data/fmriprep/wd/ica_aroma participant --skip_bids_validation --ignore slicetiming --participant-label 01 --use-aroma

# --ignore slicetiming 
# ####test
# export FS_LICENSE=/data/pt_02020/MRI_data/software/license.txt

# singularity run -B /data/pt_02020/MRI_data/Marktest/memory/ /data/pt_02020/MRI_data/software/fmriprep-1.2.5.simg /data/pt_02020/MRI_data/Marktest/memory/ /data/pt_02020/MRI_data/fmriprep/   --template MNI152NLin2009cAsym --template-resampling-grid /data/pt_02020/MRI_data/software/MNI152_T1_2mm_brain.nii.gz --fs-license-file $FS_LICENSE --cleanenv -w /data/pt_02020/MRI_data/fmriprep/wd participant --participant-label 01 &



## FMRIPREP
### fmriprep /data/pt_02020/MRI_data/Marktest/memory/ /data/pt_02020/MRI_data/fmriprep/ participant --participant-label 01




# # 
# # 
# # ##### additional info
# # fmriprep [-h] [--version]
# #                 [--participant_label PARTICIPANT_LABEL [PARTICIPANT_LABEL ...]]
# #                 [-t TASK_ID] [--debug] [--nthreads NTHREADS]
# #                 [--omp-nthreads OMP_NTHREADS] [--mem_mb MEM_MB] [--low-mem]
# #                 [--use-plugin USE_PLUGIN] [--anat-only]
# #                 [--ignore-aroma-denoising-errors] [-v]
# #                 [--ignore {fieldmaps,slicetiming} [{fieldmaps,slicetiming} ...]]
# #                 [--longitudinal] [--t2s-coreg] [--bold2t1w-dof {6,9,12}]
# #                 [--output-space {T1w,template,fsnative,fsaverage,fsaverage6,fsaverage5} [{T1w,template,fsnative,fsaverage,fsaverage6,fsaverage5} ...]]
# #                 [--force-bbr] [--force-no-bbr]
# #                 [--template {MNI152NLin2009cAsym}]
# #                 [--output-grid-reference OUTPUT_GRID_REFERENCE]
# #                 [--template-resampling-grid TEMPLATE_RESAMPLING_GRID]
# #                 [--medial-surface-nan] [--use-aroma]
# #                 [--aroma-melodic-dimensionality AROMA_MELODIC_DIMENSIONALITY]
# #                 [--skull-strip-template {OASIS,NKI}] [--fmap-bspline]
# #                 [--fmap-no-demean] [--use-syn-sdc] [--force-syn]
# #                 [--fs-license-file PATH] [--no-submm-recon]
# #                 [--cifti-output | --fs-no-reconall] [-w WORK_DIR]
# #                 [--resource-monitor] [--reports-only] [--run-uuid RUN_UUID]
# #                 [--write-graph] [--stop-on-first-crash] [--notrack]
# #                 bids_dir output_dir {participant}
