Readme file for preprocessing of GBS data:

Script authors: medawar@cbs.mpg.de; thieleking@cbs.mpg.de
Readme author: jensen@cbs.mpg.de


1. run preprocessing using fmriprep from the command line.
Script: fmri-prep.sh

2. Run postfmriprep_smoothing.m with associated *_job.m file


3. Quality Controll in with how-to guide /data/pt_02020/Data/01_preprocessing_step-1/fmriprep/QA/
Script: MISSING ???

4. use FSL to ectract motion outliers in folder 1_fsl_motion_outliers


