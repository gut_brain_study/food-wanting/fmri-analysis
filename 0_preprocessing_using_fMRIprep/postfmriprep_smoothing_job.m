%-----------------------------------------------------------------------
% Job saved on 07-Apr-2021 12:55:04 by cfg_util (rev $Rev: 6942 $)
% spm SPM - SPM12 (7219)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
% clc; clear all

% initialise SPM defaults
spm('defaults', 'FMRI');
spm_jobman('initcfg');


dataDir = '/data/pt_02020/MRI_data/fmriprep/fmriprep/';


subjList = [01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61];
sesList = [01 02 03 04];
taskList = ["wanting","memory"];


for subj = subjList(48):subjList(61)
    for j = sesList(1):sesList(4) %1:numel(sesList) %ses
            
         subj = num2str(subj, '%02d'); % zero-pads variables to be 2 characters long
         ses = num2str(j, '%02d');
         fprintf('subj-%s session-%s', subj, ses)
        
         for tasknum = 1:length(taskList)
         task = taskList{tasknum};


            %% check if session is missing, then skip
            if exist([dataDir 'sub-' subj '/ses-' ses '/func/']) == 0
                continue 
            end

            %% check if task is missing, then skip
            if exist([dataDir 'sub-' subj '/ses-' ses '/func/sub-' subj '_ses-' ses '_task-' task '_space-MNI152NLin2009cAsym_desc-preproc_bold.nii.gz']) == 0
                continue 
            end

        %%% gunzip nii.gz files from BIDS format after fmriprep
        % target file: /data/pt_02020/MRI_data/fmriprep/fmriprep/sub-01/ses-01/func/sub-01_ses-01_task-wanting_space-MNI152NLin2009cAsym_desc-preproc_bold.nii.gz
        %% for available sessions unzip, unless .nii is already unzipped
        if exist([dataDir 'sub-' subj '/ses-' ses '/func/sub-' subj '_ses-' ses '_task-' task '_space-MNI152NLin2009cAsym_desc-preproc_bold.nii']) == 0
             display('BOLD task has not been unzipped; unzipping now')
                gunzip([dataDir 'sub-' subj '/ses-' ses '/func/sub-' subj '_ses-' ses '_task-' task '_space-MNI152NLin2009cAsym_desc-preproc_bold.nii.gz'])
                %% delete original .nii.gz
                delete([dataDir 'sub-' subj '/ses-' ses '/func/sub-' subj '_ses-' ses '_task-' task '_space-MNI152NLin2009cAsym_desc-preproc_bold.nii.gz'])
                display('original .nii.gz deleted')

        else
          display('BOLD task is already unzipped; doing nothing')
        end   


        %% check if already smoothed, then skip
        if exist([dataDir 'sub-' subj '/ses-' ses '/func/ssub-' subj '_ses-' ses '_task-' task '_space-MNI152NLin2009cAsym_desc-preproc_bold.nii']);
              display('BOLD task is already smoothed; doing nothing')
            continue 
        end

        %% define input nii to be smoothed
        input_nii=([dataDir 'sub-' subj '/ses-' ses '/func/sub-' subj '_ses-' ses '_task-' task '_space-MNI152NLin2009cAsym_desc-preproc_bold.nii']);


        matlabbatch{1}.spm.spatial.smooth.data = cellstr(input_nii);
        matlabbatch{1}.spm.spatial.smooth.fwhm = [6 6 6];
        matlabbatch{1}.spm.spatial.smooth.dtype = 0;
        matlabbatch{1}.spm.spatial.smooth.im = 0;
        matlabbatch{1}.spm.spatial.smooth.prefix = 's';


        spm_jobman('run', matlabbatch);

%             % uncomment for deleting unzipped .nii
%             if exist([dataDir 'sub-' subj '/ses-' ses '/func/sub-' subj '_ses-' ses '_task-' task '_space-MNI152NLin2009cAsym_desc-preproc_bold.nii'])
%              display('BOLD task.nii still exists; deleting now')
%                 delete([dataDir 'sub-' subj '/ses-' ses '/func/sub-' subj '_ses-' ses '_task-' task '_space-MNI152NLin2009cAsym_desc-preproc_bold.nii'])
%             else
%               display('BOLD task.nii not found; doing nothing')
%             end 


        end
    end
end