## GUT-BRAIN study
#### Plotting 2nd level fMRI Food Wanting results
## medawar 2021-09-28

#########################
##### PRE-REQUISITES ####
###start python environment (2.7 institute's version is enough) 
## connect to compute server 
# getserver -Ls
# python2
###### start virtualenv
# cd /data/pt_02020/MRI_data/software/taskfmri
# source bin/activate
# python /data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/5_plotting/nilearn_plotting.py

import os
import nilearn
from nilearn import plotting
import numpy as np
import nibabel as nib
from nibabel.testing import data_path
#import matplotlib as mpl
#import matplotlib.pyplot as plt
#from matplotlib import cm

## set standard brain background image
#cavg152T1 = '/afs/cbs.mpg.de/software/fsl/6.0.3/ubuntu-bionic-amd64/data/standard/MNI152_T1_1mm_brain.nii.gz'

######## plot 2nd level masks ###################
### plot neurosynth_raw + neurosynth_peak masks 2nd level onto glass brain
ns_raw = '/data/pt_02020/MRI_data/software/ROI_analysis/neurosynth/reward_hypothalamus_merged_bin.nii'
ns_peak = '/data/pt_02020/MRI_data/software/ROI_analysis/neurosynth_peak/reward_hypothalamus_merged_spheres_only.nii'

display=plotting.plot_glass_brain(ns_raw, display_mode='lyrz', colorbar=False, figure=None, axes=None, title='reward + hypothalamus map (neurosynth)', threshold=0, annotate=True, black_bg=False, cmap='plasma', alpha=0.7, vmin=None, vmax=None, plot_abs=True, symmetric_cbar='auto', resampling_interpolation='continuous')
display.savefig('/data/pt_02020/MRI_data/fmri_wanting_results/4_plots/neurosynthraw_mask.pdf')
#display = plotting.plot_glass_brain(None)
#display.add_contours(ns_raw, filled=True, display_mode='lyrz', colors='r')

display=plotting.plot_glass_brain(ns_peak, display_mode='lyrz', colorbar=False, figure=None, axes=None, title='reward + hypothalamus map peaks only (neurosynth)', threshold=0, annotate=True, black_bg=False, cmap='plasma', alpha=0.7, vmin=None, vmax=None, plot_abs=True, symmetric_cbar='auto', resampling_interpolation='continuous')
display.savefig('/data/pt_02020/MRI_data/fmri_wanting_results/4_plots/neurosynthpeak_mask.pdf')

######## To Do: plot 2nd level results used in network analysis ###################
### interaction effects in VTA and rOFC
m1 = '/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/no_confound/allsubs/neurosynthraw/interaction/modelB1/con_0001/swe_tfce_c01.nii'
m1_deact = '/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/no_confound/allsubs/neurosynthraw/interaction/modelB1/con_0002/swe_tfce_c02.nii'

#c1 ='/data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_VTA.nii.gz'
#c2 ='/data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_rOFC.nii.gz'
#c3 ='/data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_rmOFC.nii.gz'
#c4 ='/data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0002_cluster_VTA_deact.nii.gz'
#
#### main effect of Food Wanting (model A, con 01, 03, 05)
#m2 = '/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/no_confound/allsubs/neurosynthraw/main/modelA/con_0001/swe_tfce_c01.nii'
#m3 = '/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/no_confound/allsubs/neurosynthraw/main/modelA/con_0003/swe_tfce_c01.nii'
#m4 = '/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/no_confound/allsubs/neurosynthraw/main/modelA/con_0005/swe_tfce_c01.nii'
#### peak voxel spheres reward and hypothalamus network
ns_peak = '/data/pt_02020/MRI_data/software/ROI_analysis/neurosynth_peak/reward_hypothalamus_merged_spheres_only.nii'

display=plotting.plot_glass_brain(m1, display_mode='lyrz', colorbar=False, figure=None, axes=None, title='individual values extracted', threshold=180, annotate=True, black_bg=False, cmap='plasma', alpha=0.7, vmin=None, vmax=None, plot_abs=True, symmetric_cbar='auto', resampling_interpolation='continuous')
display.add_overlay(m1_deact, cmap='Blues', threshold=180) 
#display.add_overlay(m2, cmap=plotting.cm.purple_green, threshold=0) 
#display.add_overlay(m3, cmap=plotting.cm.purple_green, threshold=0) 
#display.add_overlay(m4, cmap=plotting.cm.purple_green, threshold=0) 
display.savefig('/data/pt_02020/MRI_data/fmri_wanting_results/4_plots/fmri_values_extracted.pdf')

#display=plotting.plot_roi(m1, bg_img=avg152T1, cut_coords=None, output_file=None, display_mode='ortho', figure=None, axes=None, title=None, annotate=True, draw_cross=True, black_bg='auto', threshold=0.5, alpha=0.7, cmap=plotting.cm.purple_green, dim='auto', vmin=None, vmax=None, resampling_interpolation='nearest', view_type='continuous', linewidths=2.5)


# ######## plot RESULTS ###################
##### MAIN EFFECTS ###### thresholded at TFCE 50
# ### loop over paths + models
# for confound in ['no_confound', 'incl_confound', 'fiber', 'fm','posthunger','prehunger','ses', 'vaiak', 'wellbeing']: ####  
#     for subs in ['allsubs','excluded_sub-30','excluded_sub-30_sub-47_ses-04','excluded_sub-47_ses-04','only_female','only_male']: ###
#         for mask in ['neurosynthraw','neurosynthpeak','nomask']: ####
#             for effect in ['main']: ##
#                 #for model in ['modelB2']:
#                 for model in ['modelB1','modelA','modelB2','modelC1','modelC2']: ####
#                     for con in ['con_0001','con_0002','con_0003','con_0004','con_0005']: ###
#                         f1='/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/%s/%s/%s/%s/%s/%s/swe_tfce_c01.nii' %(confound,subs,mask,effect,model,con)
#                         f1_tstat='/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/%s/%s/%s/%s/%s/%s/swe_vox_zTstat-WB_c01.nii' %(confound,subs,mask,effect,model,con) ## for Tstat map use: swe_vox_zTstat-WB_c01.nii
#                         f2='/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/%s/%s/%s/%s/%s/%s/swe_tfce_lpFWE-WB_c01.nii' %(confound,subs,mask,effect,model,con)
#                         title = '%s %s %s' %(effect,model,con)
#                         if os.path.isfile(f1):

# #f1='/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/no_confound/allsubs/neurosynthraw/interaction/modelB1/con_0001/swe_tfce_c01.nii'
# #f2='/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/no_confound/allsubs/neurosynthraw/interaction/modelB1/con_0001/swe_tfce_lpFWE-WB_c01.nii'
# #f1 = '/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/no_confound/allsubs/neurosynthraw/main/modelA/con_0004/swe_tfce_c01.nii'
# #f2 = '/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/no_confound/allsubs/neurosynthraw/main/modelA/con_0004/swe_tfce_lpFWE-WB_c01.nii'                        
#                             print(f1)
#                             print(f2)
#                             ### plot unthresholded contrast on glass brain; minimal threshold set to t = 1.3  -> more conventional: no threshold for t-value
#                             display=plotting.plot_glass_brain(f1, display_mode='lyrz', colorbar=True, figure=None, axes=None, title=title, threshold=50, annotate=True, black_bg=False, cmap='plasma', alpha=0.7, vmin=None, vmax=None, plot_abs=True, symmetric_cbar='auto', resampling_interpolation='continuous') ##, cbar_tick_format = '%i'
#                             #####display=plotting.plot_glass_brain(f1_tstat, display_mode='lyrz', colorbar=True, figure=None, axes=None, title=title, threshold=2.3, annotate=True, black_bg=False, cmap='plasma', alpha=0.7, vmin=None, vmax=None, plot_abs=True, symmetric_cbar='auto', resampling_interpolation='continuous')

#                  			  ##work in progress:
#                             #n standard brain --> threshold default 1e-06
#                             ##display=plotting.plot_stat_map(f1, bg_img=avg152T1, cut_coords=[-4, -20, -14], output_file=None, display_mode='ortho', colorbar=True, figure=None, axes=None, title=None, threshold=1e-06, annotate=True, draw_cross=True, black_bg='auto', symmetric_cbar='auto', dim='auto', vmax=None, resampling_interpolation='continuous') 
 			
 			    
#                 			 ### CONDITIONAL LOOP: for f2, check if any p-value is > 1.3 (p-FWE = 0.05), if not: skip plotting contours
#             			     # make array of image
#             			     #example_f2 = os.path.join(data_path, f2)
#                             f2_img = nib.load(f2)
#                             f2_data=f2_img.get_fdata()
#                             if np.any(f2_data > 1.3):
#                                 ### add contours for p-FWE < 0.05 (log10(0.05) = 1.3)				
#                                 display.add_contours(f2, threshold=1.3,levels=[1.3], colors='r', filled=False)
#                             else: 
#                                 print("no significant voxels found")

#                             ### save output file 
#                             ##### set automatic output path
#                             filename = '%s_%s_%s_%s_%s_%s_swe_tfce_c01' %(confound,subs,mask,effect,model,con)
#                             display.savefig('/data/pt_02020/MRI_data/fmri_wanting_results/4_plots/%s.pdf' %(filename))
#                             #display.savefig('/data/pt_02020/MRI_data/fmri_wanting_results/4_plots/standardbrain/%s.pdf' %(filename))

                            
#                             #filename = '%s_%s_%s_%s_%s_%s_swe_vox_zTstat-WB_c01.nii' %(confound,subs,mask,effect,model,con) ###output name for Tstat map
#                             #display.savefig('/data/pt_02020/MRI_data/fmri_wanting_results/4_plots/zTstat_maps/%s.pdf' %(filename))
     
######## INTERACTION EFFECTS (thresholded at TFCE = 0) ############
### loop over paths + models
for confound in ['no_confound', 'incl_confound', 'fiber', 'fm','posthunger','prehunger','ses', 'vaiak', 'wellbeing']: ####  
    for subs in ['allsubs','excluded_sub-30','excluded_sub-30_sub-47_ses-04','excluded_sub-47_ses-04','only_female','only_male']: ###
        for mask in ['neurosynthraw','neurosynthpeak','nomask']: ####
            for effect in ['interaction']:
                for model in ['modelB1','modelA','modelB2','modelC1','modelC2']: ####
                    for con in ['con_0001','con_0002','con_0003','con_0004','con_0005']: ###
                        f1='/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/%s/%s/%s/%s/%s/%s/swe_tfce_c01.nii' %(confound,subs,mask,effect,model,con)
                        f1_tstat='/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/%s/%s/%s/%s/%s/%s/swe_vox_zTstat-WB_c01.nii' %(confound,subs,mask,effect,model,con) ## for Tstat map use: swe_vox_zTstat-WB_c01.nii
                        f2='/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/%s/%s/%s/%s/%s/%s/swe_tfce_lpFWE-WB_c01.nii' %(confound,subs,mask,effect,model,con)
                        title = '%s %s %s' %(effect,model,con)
                        if os.path.isfile(f1):

#f1='/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/no_confound/allsubs/neurosynthraw/interaction/modelB1/con_0001/swe_tfce_c01.nii'
#f2='/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/no_confound/allsubs/neurosynthraw/interaction/modelB1/con_0001/swe_tfce_lpFWE-WB_c01.nii'
#f1 = '/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/no_confound/allsubs/neurosynthraw/main/modelA/con_0004/swe_tfce_c01.nii'
#f2 = '/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/no_confound/allsubs/neurosynthraw/main/modelA/con_0004/swe_tfce_lpFWE-WB_c01.nii'                        
                            print(f1)
                            print(f2)
                            ### plot unthresholded contrast on glass brain; minimal threshold set to t = 1.3  -> more conventional: no threshold for t-value
                            display=plotting.plot_glass_brain(f1, display_mode='lyrz', colorbar=True, figure=None, axes=None, title=title, threshold=0, annotate=True, black_bg=False, cmap='plasma', alpha=0.7, vmin=None, vmax=None, plot_abs=True, symmetric_cbar='auto', resampling_interpolation='continuous') ##, cbar_tick_format = '%i'
                            #####display=plotting.plot_glass_brain(f1_tstat, display_mode='lyrz', colorbar=True, figure=None, axes=None, title=title, threshold=2.3, annotate=True, black_bg=False, cmap='plasma', alpha=0.7, vmin=None, vmax=None, plot_abs=True, symmetric_cbar='auto', resampling_interpolation='continuous')
 			    
                			 ### CONDITIONAL LOOP: for f2, check if any p-value is > 1.3 (p-FWE = 0.05), if not: skip plotting contours
            			     # make array of image
            			     #example_f2 = os.path.join(data_path, f2)
                            f2_img = nib.load(f2)
                            f2_data=f2_img.get_fdata()
                            if np.any(f2_data > 1.3):
                                ### add contours for p-FWE < 0.05 (log10(0.05) = 1.3)				
                                display.add_contours(f2, threshold=1.3,levels=[1.3], colors='r', filled=False)
                            else: 
                                print("no significant voxels found")

                            ### save output file 
                            ##### set automatic output path
                            filename = '%s_%s_%s_%s_%s_%s_swe_tfce_c01' %(confound,subs,mask,effect,model,con)
                            display.savefig('/data/pt_02020/MRI_data/fmri_wanting_results/4_plots/%s.pdf' %(filename))
                            #display.savefig('/data/pt_02020/MRI_data/fmri_wanting_results/4_plots/standardbrain/%s.pdf' %(filename))

                            
                            #filename = '%s_%s_%s_%s_%s_%s_swe_vox_zTstat-WB_c01.nii' %(confound,subs,mask,effect,model,con) ###output name for Tstat map
                            #display.savefig('/data/pt_02020/MRI_data/fmri_wanting_results/4_plots/zTstat_maps/%s.pdf' %(filename))
              


# ### deactivation: specific intervention effects
# f1='/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/no_confound/allsubs/neurosynthraw/interaction/modelB1/con_0002/swe_tfce_c02.nii' 
# f2='/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/no_confound/allsubs/neurosynthraw/interaction/modelB1/con_0002/swe_tfce_lpFWE-WB_c02.nii'
# title = 'interaction_modelB1_con_0002_deact'
# display=plotting.plot_glass_brain(f1, display_mode='lyrz', colorbar=True, figure=None, axes=None, title=title, threshold=50, annotate=True, black_bg=False, cmap='Blues', alpha=0.7, vmin=None, vmax=None, plot_abs=True, symmetric_cbar='auto', resampling_interpolation='continuous')
# display.add_contours(f2, threshold=1.3,levels=[1.3], colors='r', filled=False)
# filename = 'no_confound_allsubs_neurosynthraw_interaction_modelB1_con_0002_swe_tfce_c02'
# display.savefig('/data/pt_02020/MRI_data/fmri_wanting_results/4_plots/%s.pdf' %(filename))


# #### for quick check (in terminal)
# ## plotting.show()
                            
                        
