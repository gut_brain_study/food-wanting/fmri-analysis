## GUT-BRAIN study
#### Plotting 2nd level fMRI Food Wanting results
## script from medawar 2021-09-28 and edited by Daria Jensen, Nov 2023

#########################
##### PRE-REQUISITES ####
#pip install -U --user nilearn

###start python environment (2.7 institute's version is enough) 
## connect to compute server 
# getserver -sL
# cd /data/p_02020/Software/taskfmri
# source bin/activate
# python2
###### start virtualenv
# python /data/pt_02020/Analysis/MRI/1_FUNCTIONAL/5_plotting/nilearn_plotting_memory.py


import os
import nilearn
from nilearn import plotting
import numpy as np
import nibabel as nib
#from nibabel.testing import data_path

#import matplotlib as mpl
#import matplotlib.pyplot as plt
#from matplotlib import cm

##################### SETTINGS ####################################
## set standard brain background image
#cavg152T1 = '/afs/cbs.mpg.de/software/fsl/6.0.3/ubuntu-bionic-amd64/data/standard/MNI152_T1_1mm_brain.nii.gz'
d="/data/pt_02020/Results/fmri_memory/";


##################### ALL RESULTS ####################################

#test script first:
# confound='no_confound'
# subs='allsubs'
# mask='nomask'
# effect='main'
# model='encoding_wanting'
# con='con_0001'
# f1=(d + '2nd_level/%s/%s/%s/%s/%s/%s/swe_tfce_c01.nii') %(confound,subs,mask,effect,model,con)
# f1_tstat=(d + '2nd_level/%s/%s/%s/%s/%s/%s/swe_vox_zTstat-WB_c01.nii') %(confound,subs,mask,effect,model,con) ## for Tstat map use: swe_vox_zTstat-WB_c01.nii
# f2=(d + '2nd_level/%s/%s/%s/%s/%s/%s/swe_tfce_lpFWE-WB_c01.nii') %(confound,subs,mask,effect,model,con)
# title = '%s %s %s' %(effect,model,con)
# if os.path.isfile(f1):                   
#     print(f1)
#     print(f2)
#     display=plotting.plot_glass_brain(f1, display_mode='lyrz', colorbar=True, figure=None, axes=None, title=title, threshold=50, annotate=True, black_bg=False, cmap='plasma', alpha=0.7, vmin=None, vmax=None, plot_abs=True, symmetric_cbar='auto', resampling_interpolation='continuous') ##, cbar_tick_format = '%i'
#     f2_img = nib.load(f2)
#     f2_data=f2_img.get_fdata()
#     if np.any(f2_data > 1.3):		
#         display.add_contours(f2, threshold=1.3,levels=[1.3], colors='r', filled=False)
#     else: 
#         print("no significant voxels found")
#     filename = '%s_%s_%s_%s_%s_%s_swe_tfce_c01' %(confound,subs,mask,effect,model,con)
#     display.savefig((d + '4_plots/%s.pdf') %(filename))

                            

    
## check which files are missing:
from pathlib import Path

cons={'01','02'} # min cons available for an overview
# check all:
for model in [ 'encoding','encoding_wanting','encoding_kcal','encoding_fibre','modelA','memoryRecall','memoryRecall_hitmiss','memoryRecall_OldSimNew','memoryWanting','fibre','kcal']:
    for confound in ['incl_confound', 'no_confound']:#, 'fiber', 'fm', 'posthunger', 'prehunger', 'ses', 'vaiak', 'wellbeing'
        for subs in ['allsubs']:#, 'only_female', 'only_male'
            for mask in ['subcortical']:#,,,'nomask''myrois','HTreward'
                for effect in ['main', 'interaction']:
                    for i in cons:  # ['con_0001','con_0002']:
                        con = ('con_00'+i)#cons =[5 3 2 2 5 6 8 18 3 2 2];
                        f1 = (d + '2nd_level/%s/%s/%s/%s/%s/%s/swe_tfce_c01.nii') % (confound, subs, mask, effect, model, con)
                        
                        #print(os.path.exists(f1))
                        path=Path(f1)
                        if path.is_file()==False:
                            print(f1)
      
        
## max cons available                   
cons=list(range(1, 19))
cons=["{:02d}".format(item) for item in cons] # 2 decimal 
for i in cons:
    con=('con_00'+i)      

######################
### ALL FIGURES ######
######################

#### MAIN EFFECTS ###### thresholded at TFCE 50
for model in ['encoding','encoding_wanting','encoding_kcal','encoding_fibre','modelA','memoryRecall','memoryRecall_hitmiss','memoryRecall_OldSimNew','memoryWanting','fibre','kcal']:
    for confound in ['incl_confound']:#, 'no_confound', 'fiber', 'fm', 'posthunger', 'prehunger', 'ses', 'vaiak', 'wellbeing'
        for subs in ['allsubs']:#, 'only_female', 'only_male'
            for mask in ['myrois','subcortical','HTreward',]:  # 'nomask'
                for effect in ['main']:
                    for i in cons:  # ['con_0001','con_0002']:
                        con = ('con_00'+i)#cons =[5 3 2 2 5 6 8 18 3 2 2];
                        f1 = (d + '2nd_level/%s/%s/%s/%s/%s/%s/swe_tfce_c01.nii') % (confound, subs, mask, effect, model, con)
                        f1_tstat = (d + '2nd_level/%s/%s/%s/%s/%s/%s/swe_vox_zTstat-WB_c01.nii') % (confound, subs, mask, effect, model, con)  # for Tstat map use: swe_vox_zTstat-WB_c01.nii
                        f2 = (d + '2nd_level/%s/%s/%s/%s/%s/%s/swe_tfce_lpFWE-WB_c01.nii') % (confound, subs, mask, effect, model, con)
                        title = '%s %s %s' % (effect, model, con)
                        if os.path.isfile(f1):
                              print(f1)
                              print(f2)
                              # plot unthresholded contrast on glass brain; minimal threshold set to t = 1.3  -> more conventional: no threshold for t-value
                              display = plotting.plot_glass_brain(f1, display_mode='lyrz', colorbar=True, figure=None, axes=None, 
                                                                   title=title, threshold=0, annotate=True, black_bg=False,
                                                                   cmap='plasma', alpha=0.7, vmin=None, vmax=None, plot_abs=True, symmetric_cbar='auto', 
                                                                   resampling_interpolation='continuous')  # , cbar_tick_format = '%i'
                              # display=plotting.plot_glass_brain(f1_tstat,display_mode='lyrz', colorbar=True, figure=None, axes=None,
                              #                                   title=title, threshold=2.3,
                              #                                   annotate=True, black_bg=False, cmap='plasma',
                              #                                   alpha=0.7, vmin=None, vmax=None, plot_abs=True, symmetric_cbar='auto',
                              #                                   resampling_interpolation='continuous')
                     		  #TODO: standard brain --> threshold default 1e-06
                              #display=plotting.plot_stat_map(f1, bg_img=avg152T1, 
                              #     cut_coords=[-4, -20, -14], output_file=None, 
                              #     display_mode='ortho', colorbar=True, figure=None, axes=None,
                              #     title=None, threshold=1e-06, annotate=True, draw_cross=True, 
                              #     black_bg='auto', symmetric_cbar='auto', dim='auto', vmax=None, 
                              #     resampling_interpolation='continuous')

                			  # make array of image #f2 = os.path.join(data_path, f2)
                              f2_img = nib.load(f2)
                              f2_data = f2_img.get_fdata()
                              if np.any(f2_data > 1.3):# CONDITIONAL LOOP: for f2, check if any p-value is > 1.3 (p-FWE = 0.05), if not: skip plotting contours
                                  # add contours for p-FWE < 0.05 (log10(0.05) = 1.3)
                                  display.add_contours(f2, threshold=1.3, levels=[1.3], colors='r', filled=False)
                              else:
                                  print("no significant voxels found")
                              # save output file
                              filename = '%s_%s_%s_%s_%s_%s_swe_tfce_c01' % (confound, subs, mask, effect,model,con)
                              #filename = 'zTstat/%s_%s_%s_%s_%s_%s_swe_vox_zTstat-WB_c01.nii' %(confound,subs,mask,effect,model,con) #output name for Tstat map
                              #filename = 'standardbrain/%s_%s_%s_%s_%s_%s_swe_vox_zTstat-WB_c01.nii' %(confound,subs,mask,effect,model,con)
                              display.savefig((d + '4_plots/%s.pdf') % (filename))

######## INTERACTION EFFECTS (thresholded at TFCE = 0) ############
### loop over paths + models
### 'encoding','encoding_wanting','encoding_kcal','encoding_fibre','modelA','memoryRecall','memoryRecall_hitmiss','fibre','kcal','memoryRecall_OldSimNew',
for model in ['memoryWanting' ]:
    for confound in [ 'incl_confound']: ####  , 'no_confound','fiber', 'fm','posthunger','prehunger','ses', 'vaiak', 'wellbeing'
        for subs in ['allsubs']: #,'only_female','only_male'
            for mask in ['myrois','subcortical','HTreward']: # ,'nomask',
                for effect in ['interaction']:#
                    for i in cons:
                        con=('con_00'+i)    
                        f1=(d + '2nd_level/%s/%s/%s/%s/%s/%s/swe_tfce_c01.nii') %(confound,subs,mask,effect,model,con)
                        f1_tstat=(d  + '2nd_level/%s/%s/%s/%s/%s/%s/swe_vox_zTstat-WB_c01.nii') %(confound,subs,mask,effect,model,con) ## for Tstat map use: swe_vox_zTstat-WB_c01.nii
                        f2=(d  + '2nd_level/%s/%s/%s/%s/%s/%s/swe_tfce_lpFWE-WB_c01.nii') %(confound,subs,mask,effect,model,con)
                        title = '%s %s %s' %(effect,model,con)
                        if os.path.isfile(f1):                       
                            print(f1)
                            print(f2)
                            # plot unthresholded contrast on glass brain; minimal threshold set to t = 1.3  -> more conventional: no threshold for t-value
                            display=plotting.plot_glass_brain(f1, 
                                                              display_mode='lyrz', colorbar=True, figure=None, axes=None, 
                                                              title=title, threshold=0, annotate=True, black_bg=False, cmap='plasma', 
                                                              alpha=0.7, vmin=None, vmax=None, plot_abs=True, symmetric_cbar='auto', 
                                                              resampling_interpolation='continuous') ##, cbar_tick_format = '%i'
                            # display=plotting.plot_glass_brain(f1_tstat, 
                            #                                 display_mode='lyrz', colorbar=True, figure=None, axes=None, 
                            #                                 title=title, threshold=2.3, annotate=True, black_bg=False, cmap='plasma', 
                            #                                 alpha=0.7, vmin=None, vmax=None, plot_abs=True, symmetric_cbar='auto', 
                            #                                 resampling_interpolation='continuous')
            			    # make array of image
                            f2_img = nib.load(f2) 
                            f2_data=f2_img.get_fdata()
                            if np.any(f2_data > 1.3): # CONDITIONAL LOOP: for f2, check if any p-value is > 1.3 (p-FWE = 0.05), if not: skip plotting contours
                                # add contours for p-FWE < 0.05 (log10(0.05) = 1.3)				
                                display.add_contours(f2, threshold=1.3,levels=[1.3], colors='r', filled=False)
                            else: 
                                print("no significant voxels found")
                            # save output file 
                            filename = '%s_%s_%s_%s_%s_%s_swe_tfce_c01' %(confound,subs,mask,effect,model,con)
                            #filename = 'zTstat/%s_%s_%s_%s_%s_%s_swe_vox_zTstat-WB_c01.nii' %(confound,subs,mask,effect,model,con) ###output name for Tstat map
                            display.savefig((d + '4_plots/%s.pdf' ) %(filename))


#################################################
## Save just when significant vx were found:
##########################################
for model in ['encoding_wanting','memoryWanting','encoding','encoding_kcal','encoding_fibre','modelA','memoryRecall','memoryRecall_hitmiss','memoryRecall_OldSimNew','fibre','kcal']:
    for confound in ['incl_confound']:#, 'no_confound', 'fiber', 'fm', 'posthunger', 'prehunger', 'ses', 'vaiak', 'wellbeing'
        for subs in ['allsubs']:#, 'only_female', 'only_male'
            for mask in ['myrois']:  # ,'subcortical''HTreward',,'nomask'
                for effect in ['main']:
                    for i in cons:  # ['con_0001','con_0002']:
                        con = ('con_00'+i)#cons =[5 3 2 2 5 6 8 18 3 2 2];
                        f1 = (d + '2nd_level/%s/%s/%s/%s/%s/%s/swe_tfce_c01.nii') % (confound, subs, mask, effect, model, con)
                        f1_tstat = (d + '2nd_level/%s/%s/%s/%s/%s/%s/swe_vox_zTstat-WB_c01.nii') % (confound, subs, mask, effect, model, con)  # for Tstat map use: swe_vox_zTstat-WB_c01.nii
                        f2 = (d + '2nd_level/%s/%s/%s/%s/%s/%s/swe_tfce_lpFWE-WB_c01.nii') % (confound, subs, mask, effect, model, con)
                        title = '%s %s %s' % (effect, model, con)
                        if os.path.isfile(f1):
                			  # make array of image #f2 = os.path.join(data_path, f2)
                              f2_img = nib.load(f2)
                              f2_data = f2_img.get_fdata()
                              
                              if np.any(f2_data > 1.3):# CONDITIONAL LOOP: for f2, check if any p-value is > 1.3 (p-FWE = 0.05), if not: skip plotting contours
                                     # plot unthresholded contrast on glass brain; minimal threshold set to t = 1.3  -> more conventional: no threshold for t-value
                                     display = plotting.plot_glass_brain(f1, display_mode='lyrz', colorbar=True, figure=None, axes=None, 
                                                                   title=title, threshold=0, annotate=True, black_bg=False,
                                                                   cmap='plasma', alpha=0.7, vmin=None, vmax=None, plot_abs=True, symmetric_cbar='auto', 
                                                                   resampling_interpolation='continuous')  # , cbar_tick_format = '%i'
                                     # display=plotting.plot_glass_brain(f1_tstat,display_mode='lyrz', colorbar=True, figure=None, axes=None,
                                     #                                    title=title, threshold=2.3,
                                     #                                    annotate=True, black_bg=False, cmap='plasma',
                                     #                                    alpha=0.7, vmin=None, vmax=None, plot_abs=True, symmetric_cbar='auto',
                                     #                                    resampling_interpolation='continuous')
                                     #TODO: standard brain --> threshold default 1e-06
                                     #display=plotting.plot_stat_map(f1, bg_img=avg152T1, 
                                     #     cut_coords=[-4, -20, -14], output_file=None, 
                                     #     display_mode='ortho', colorbar=True, figure=None, axes=None,
                                     #     title=None, threshold=1e-06, annotate=True, draw_cross=True, 
                                     #     black_bg='auto', symmetric_cbar='auto', dim='auto', vmax=None, 
                                     #     resampling_interpolation='continuous')
                                    
                                     # add contours for p-FWE < 0.05 (log10(0.05) = 1.3)
                                     display.add_contours(f2, threshold=1.3, levels=[1.3], colors='r', filled=False)                                  
                                     # save output file
                                     filename = 'significant/%s_%s_%s_%s_%s_%s_swe_tfce_c01' % (confound, subs, mask, effect,model,con)
                                     #filename = 'significant/%s_%s_%s_%s_%s_%s_swe_vox_zTstat-WB_c01.nii' %(confound,subs,mask,effect,model,con) #output name for Tstat map
                                     #filename = 'standardbrain/%s_%s_%s_%s_%s_%s_swe_vox_zTstat-WB_c01.nii' %(confound,subs,mask,effect,model,con)
                                     display.savefig((d + '4_plots/%s.pdf') % (filename))
                                     print("significant:")
                                     print(f1)
                              else:
                                     print("no significant voxels found")
###### INTERACTION ##########                                     
for model in [ 'encoding','encoding_wanting','memoryRecall_OldSimNew','memoryWanting','encoding_kcal','encoding_fibre','modelA','memoryRecall','memoryRecall_hitmiss','fibre','kcal']:
    for confound in [ 'incl_confound']: ####  'no_confound', 'fiber', 'fm','posthunger','prehunger','ses', 'vaiak', 'wellbeing'
        for subs in ['allsubs']: #,'only_female','only_male'
            for mask in ['myrois']: # ,'subcortical','HTreward','nomask'
                for effect in ['interaction']:#
                    for i in cons:
                        con=('con_00'+i)    
                        f1=(d + '2nd_level/%s/%s/%s/%s/%s/%s/swe_tfce_c01.nii') %(confound,subs,mask,effect,model,con)
                        f1_tstat=(d  + '2nd_level/%s/%s/%s/%s/%s/%s/swe_vox_zTstat-WB_c01.nii') %(confound,subs,mask,effect,model,con) ## for Tstat map use: swe_vox_zTstat-WB_c01.nii
                        f2=(d  + '2nd_level/%s/%s/%s/%s/%s/%s/swe_tfce_lpFWE-WB_c01.nii') %(confound,subs,mask,effect,model,con)
                        title = '%s %s %s' %(effect,model,con)
                        if os.path.isfile(f1):
                            # make array of image
                            f2_img = nib.load(f2) 
                            f2_data=f2_img.get_fdata()
                            
                            if np.any(f2_data > 1.3): # CONDITIONAL LOOP: for f2, check if any p-value is > 1.3 (p-FWE = 0.05), if not: skip plotting contours
                                # plot unthresholded contrast on glass brain; minimal threshold set to t = 1.3  -> more conventional: no threshold for t-value
                                display=plotting.plot_glass_brain(f1, 
                                                              display_mode='lyrz', colorbar=True, figure=None, axes=None, 
                                                              title=title, threshold=0, annotate=True, black_bg=False, cmap='plasma', 
                                                              alpha=0.7, vmin=None, vmax=None, plot_abs=True, symmetric_cbar='auto', 
                                                              resampling_interpolation='continuous') ##, cbar_tick_format = '%i'
                                # display=plotting.plot_glass_brain(f1_tstat, 
                                #                                 display_mode='lyrz', colorbar=True, figure=None, axes=None, 
                                #                                 title=title, threshold=2.3, annotate=True, black_bg=False, cmap='plasma', 
                                #                                 alpha=0.7, vmin=None, vmax=None, plot_abs=True, symmetric_cbar='auto', 
                                #                                 resampling_interpolation='continuous')
                                # add contours for p-FWE < 0.05 (log10(0.05) = 1.3)				
                                display.add_contours(f2, threshold=1.3,levels=[1.3], colors='r', filled=False)
                                # save output file 
                                filename = 'significant/%s_%s_%s_%s_%s_%s_swe_tfce_c01' %(confound,subs,mask,effect,model,con)
                                #filename = 'significant/%s_%s_%s_%s_%s_%s_swe_vox_zTstat-WB_c01.nii' %(confound,subs,mask,effect,model,con) ###output name for Tstat map
                                display.savefig((d + '4_plots/%s.pdf' ) %(filename))
                                print("significant:")
                                print(f1)
                            else: 
                                print("no significant voxels found")


#####################
### deactivations: ##
#####################
# MAIN #
for model in ['encoding','encoding_kcal','encoding_fibre','modelA','memoryRecall','memoryRecall_hitmiss','memoryRecall_OldSimNew','fibre','kcal','memoryWanting','encoding_wanting']:
    for confound in ['incl_confound']:#, 'no_confound', 'fiber', 'fm', 'posthunger', 'prehunger', 'ses', 'vaiak', 'wellbeing'
        for subs in ['allsubs']:#, 'only_female', 'only_male'
            for mask in ['HTreward','subcortical','myrois','nomask']:  # 
                for effect in ['main']:
                    for i in cons:  # ['con_0001','con_0002']:
                        con = ('con_00'+i)#cons =[5 3 2 2 5 6 8 18 3 2 2];
                        f1 = (d + '2nd_level/%s/%s/%s/%s/%s/%s/swe_tfce_c02.nii') % (confound, subs, mask, effect, model, con)
                        f1_tstat = (d + '2nd_level/%s/%s/%s/%s/%s/%s/swe_vox_zTstat-WB_c02.nii') % (confound, subs, mask, effect, model, con)  # for Tstat map use: swe_vox_zTstat-WB_c01.nii
                        f2 = (d + '2nd_level/%s/%s/%s/%s/%s/%s/swe_tfce_lpFWE-WB_c02.nii') % (confound, subs, mask, effect, model, con)
                        title = 'deact %s %s %s' % (effect, model, con)
                        if os.path.isfile(f1):
                			  # make array of image #f2 = os.path.join(data_path, f2)
                              f2_img = nib.load(f2)
                              f2_data = f2_img.get_fdata()
                              if np.any(f2_data > 1.3):# CONDITIONAL LOOP: for f2, check if any p-value is > 1.3 (p-FWE = 0.05), if not: skip plotting contours
                                  display = plotting.plot_glass_brain(f1, display_mode='lyrz', colorbar=True, figure=None, axes=None, 
                                                                      title=title, threshold=0, annotate=True, black_bg=False,
                                                                      cmap='Blues', alpha=0.7, vmin=None, vmax=None, plot_abs=True, symmetric_cbar='auto', 
                                                                      resampling_interpolation='continuous')  # , cbar_tick_format = '%i'
                                  display.add_contours(f2, threshold=1.3, levels=[1.3], colors='r', filled=False)                                  
                                  filename = 'significant/%s_%s_%s_%s_%s_%s_swe_tfce_c02' % (confound, subs, mask, effect,model,con)
                                  display.savefig((d + '4_plots/%s.pdf') % (filename))
                                  print("significant:")
                                  print(f1)
                              else:
                                  print("no significant voxels found")     
                   
# INTERACTION # 
for model in ['encoding','encoding_kcal','encoding_fibre','modelA','memoryRecall','memoryRecall_hitmiss','fibre','kcal','memoryRecall_OldSimNew','memoryWanting','encoding_wanting' ]:
    for confound in [ 'incl_confound']: ####  ,'no_confound', 'fiber', 'fm','posthunger','prehunger','ses', 'vaiak', 'wellbeing'
        for subs in ['allsubs']: #,'only_female','only_male'
            for mask in ['subcortical','HTreward','myrois','nomask']: # 
                for effect in ['interaction']:#
                    for i in cons:
                        con=('con_00'+i)    
                        f1=(d + '2nd_level/%s/%s/%s/%s/%s/%s/swe_tfce_c02.nii') %(confound,subs,mask,effect,model,con)
                        f1_tstat=(d  + '2nd_level/%s/%s/%s/%s/%s/%s/swe_vox_zTstat-WB_c02.nii') %(confound,subs,mask,effect,model,con) ## for Tstat map use: swe_vox_zTstat-WB_c01.nii
                        f2=(d  + '2nd_level/%s/%s/%s/%s/%s/%s/swe_tfce_lpFWE-WB_c02.nii') %(confound,subs,mask,effect,model,con)
                        title = 'deact %s %s %s' %(effect,model,con)
                        if os.path.isfile(f1):
                            # make array of image
                            f2_img = nib.load(f2) 
                            f2_data=f2_img.get_fdata()
                            if np.any(f2_data > 1.3): # CONDITIONAL LOOP: for f2, check if any p-value is > 1.3 (p-FWE = 0.05), if not: skip plotting contours
                                # plot unthresholded contrast on glass brain; minimal threshold set to t = 1.3  -> more conventional: no threshold for t-value
                                display=plotting.plot_glass_brain(f1, 
                                                              display_mode='lyrz', colorbar=True, figure=None, axes=None, 
                                                              title=title, threshold=0, annotate=True, black_bg=False, cmap='Blues', 
                                                              alpha=0.7, vmin=None, vmax=None, plot_abs=True, symmetric_cbar='auto', 
                                                              resampling_interpolation='continuous') ##, cbar_tick_format = '%i'
                                display.add_contours(f2, threshold=1.3,levels=[1.3], colors='r', filled=False)
                                filename = 'significant/%s_%s_%s_%s_%s_%s_swe_tfce_c02' %(confound,subs,mask,effect,model,con)
                                display.savefig((d + '4_plots/%s.pdf' ) %(filename))
                                print("significant:")
                                print(f1)
                            else: 
                                print("no significant voxels found")   





#####################
######## hypothalamus stat maps #############
#####################
# MAIN #
# for model in ['encoding','encoding_kcal','encoding_fibre','modelA','memoryRecall','memoryRecall_hitmiss','memoryRecall_OldSimNew','fibre','kcal','memoryWanting','encoding_wanting']:
#     for confound in ['incl_confound']:#, 'no_confound', 'fiber', 'fm', 'posthunger', 'prehunger', 'ses', 'vaiak', 'wellbeing'
#         for subs in ['allsubs']:#, 'only_female', 'only_male'
#             for mask in ['myrois','HTreward','subcortical']:  # 'nomask'
#                 for effect in ['main']:
#                     for i in cons:  # ['con_0001','con_0002']:
#                         con = ('con_00'+i)#cons =[5 3 2 2 5 6 8 18 3 2 2];
#                         f1 = (d + '2nd_level/%s/%s/%s/%s/%s/%s/swe_tfce_c01.nii') % (confound, subs, mask, effect, model, con)
#                         f1_tstat = (d + '2nd_level/%s/%s/%s/%s/%s/%s/swe_vox_zTstat-WB_c01.nii') % (confound, subs, mask, effect, model, con)  # for Tstat map use: swe_vox_zTstat-WB_c01.nii
#                         f2 = (d + '2nd_level/%s/%s/%s/%s/%s/%s/swe_tfce_lpFWE-WB_c01.nii') % (confound, subs, mask, effect, model, con)
#                         title = '%s %s %s' % (effect, model, con)
#                         if os.path.isfile(f1):
#                 			  # make array of image #f2 = os.path.join(data_path, f2)
#                               f2_img = nib.load(f2)
#                               f2_data = f2_img.get_fdata()
#                               if np.any(f2_data > 1.3):# CONDITIONAL LOOP: for f2, check if any p-value is > 1.3 (p-FWE = 0.05), if not: skip plotting contours
#                                   #standard brain --> threshold default 1e-06
#                                   display=plotting.plot_stat_map(f2, #bg_img=avg152T1
#                                           #cut_coords=[3, -3, -10], 
#                                           output_file=None, #hypothalamus right hemisphere coordinates
#                                           display_mode='ortho', colorbar=True, figure=None, axes=None,
#                                           title=None, threshold=1.3, annotate=True, draw_cross=False, #True 
#                                           black_bg='auto', symmetric_cbar='auto', dim='auto', vmax=None, 
#                                           resampling_interpolation='continuous')
#                                   #display.add_contours(f2, threshold=1.3, levels=[1.3], colors='r', filled=False)                                  
#                                   filename = 'significant/stat_%s_%s_%s_%s_%s_%s_swe_tfce_c01' % (confound, subs, mask, effect,model,con)
#                                   display.savefig((d + '4_plots/%s.pdf') % (filename))
#                                   print("significant:")
#                                   print(f1)
#                               else:
#                                   print("no significant voxels found")     
                   
### ALL###
#
for model in ['encoding','encoding_kcal','encoding_fibre','modelA','memoryRecall','memoryRecall_hitmiss','fibre','kcal','memoryRecall_OldSimNew','memoryWanting','encoding_wanting' ]:
    for confound in [ 'incl_confound']: ####  ,'no_confound', 'fiber', 'fm','posthunger','prehunger','ses', 'vaiak', 'wellbeing'
        for subs in ['allsubs']: #,'only_female','only_male'
            for mask in ['myrois','subcortical','HTreward']: # 'nomask'
                for effect in ['main','interaction']:#
                    for i in cons:
                        con=('con_00'+i)
                        f2=(d  + '2nd_level/%s/%s/%s/%s/%s/%s/swe_tfce_lpFWE-WB_c01.nii') %(confound,subs,mask,effect,model,con)
                        f2_deac=(d  + '2nd_level/%s/%s/%s/%s/%s/%s/swe_tfce_lpFWE-WB_c02.nii') %(confound,subs,mask,effect,model,con)
                        title = '%s %s %s' %(effect,model,con)
                        if os.path.isfile(f2):
                            # make array of image
                            f2_img = nib.load(f2) 
                            f2_data=f2_img.get_fdata()
                            f2_deacimg = nib.load(f2_deac) 
                            f2_deacdata=f2_deacimg.get_fdata()
                            if np.any(f2_data > 1.3): # CONDITIONAL LOOP: for f2, check if any p-value is > 1.3 (p-FWE = 0.05), if not: skip plotting contours
                                display=plotting.plot_stat_map(f2,
                                          output_file=None, display_mode='mosaic', colorbar=True, figure=None, axes=None,
                                          title=None, threshold=1.3, annotate=True, draw_cross=False, #True
                                          black_bg='auto', symmetric_cbar='auto', dim='auto', vmax=None, 
                                          resampling_interpolation='continuous')
                                display.add_contours(f2, threshold=1.3, levels=[1.3], colors='r', filled=False)    
                                
                                if np.any(f2_deacdata > 1.3):
                                    display.add_overlay(f2_deac,cmap=plotting.cm.black_blue, threshold=1.3)
                                          # output_file=None, display_mode='ortho', ,colorbar=True, figure=None, axes=None,
                                          # title=None, annotate=True, draw_cross=False, #True
                                          # black_bg='auto', symmetric_cbar='auto', dim='auto', vmax=None, 
                                          # resampling_interpolation='continuous')
                                    display.add_contours(f2_deac, threshold=1.3, levels=[1.3], colors='b', filled=False)    
                                filename = 'significant/stat_%s_%s_%s_%s_%s_%s_swe_tfce_c01' %(confound,subs,mask,effect,model,con)
                                display.savefig((d + '4_plots/%s.pdf' ) %(filename))
                                print("significant:")
                                print(f2)
                            else: 
                                if np.any(f2_deacdata > 1.3):
                                    display=plotting.plot_stat_map(f2_deac,
                                            cmap=plotting.cm.black_blue, threshold=1.3,
                                            output_file=None, display_mode='mosaic',colorbar=True, figure=None, axes=None,
                                            title=None, annotate=True, draw_cross=False, #True
                                            black_bg='auto', symmetric_cbar='auto', dim='auto', vmax=None, 
                                            resampling_interpolation='continuous')
                                    display.add_contours(f2_deac, threshold=1.3, levels=[1.3], colors='b', filled=False)    
                                    filename = 'significant/stat_%s_%s_%s_%s_%s_%s_swe_tfce_c01' %(confound,subs,mask,effect,model,con)
                                    display.savefig((d + '4_plots/%s.pdf' ) %(filename))
                                    print("significant:")
                                    print(f2_deac)
                                    
                                print("no significant voxels found")  




#####################
#### END #####
#####################



##################### EDIT THIS FIRST before running as its from evelyn copied: ####################################

# ---- from evelyn roi masks:
### plot neurosynth_raw + neurosynth_peak masks 2nd level onto glass brain
ns_raw = '/data/pt_02020/Software/ROI_analysis/neurosynth/reward_hypothalamus_merged_bin.nii'
ns_peak = '/data/pt_02020/Software/ROI_analysis/neurosynth_peak/reward_hypothalamus_merged_spheres_only.nii'

display=plotting.plot_glass_brain(ns_raw, 
                                  display_mode='lyrz', colorbar=False, figure=None, axes=None, 
                                  title='reward + hypothalamus map (neurosynth)', 
                                  threshold=0, annotate=True, black_bg=False, cmap='plasma', 
                                  alpha=0.7, vmin=None, vmax=None, plot_abs=True, symmetric_cbar='auto', 
                                  resampling_interpolation='continuous')
display.savefig([dir + '4_plots/neurosynthraw_mask.pdf'])
#display = plotting.plot_glass_brain(None)
#display.add_contours(ns_raw, filled=True, display_mode='lyrz', colors='r')

display=plotting.plot_glass_brain(ns_peak, 
                                  display_mode='lyrz', colorbar=False, figure=None, axes=None, 
                                  title='reward + hypothalamus map peaks only (neurosynth)', 
                                  threshold=0, annotate=True, black_bg=False, cmap='plasma', 
                                  alpha=0.7, vmin=None, vmax=None, plot_abs=True, symmetric_cbar='auto',
                                  resampling_interpolation='continuous')
display.savefig([dir + '4_plots/neurosynthpeak_mask.pdf'])

######## To Do: plot 2nd level results used in network analysis ###################
### interaction effects in VTA and rOFC
m1 = [dir + 'no_confound/allsubs/neurosynthraw/interaction/modelB1/con_0001/swe_tfce_c01.nii']
m1_deact = [dir + 'no_confound/allsubs/neurosynthraw/interaction/modelB1/con_0002/swe_tfce_c02.nii']
#c1 ='/data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_VTA.nii.gz'
#c2 ='/data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_rOFC.nii.gz'
#c3 ='/data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0001_cluster_rmOFC.nii.gz'
#c4 ='/data/pt_02020/MRI_data/fmri_wanting_results/3_rawvalues/modelB1_con_0002_cluster_VTA_deact.nii.gz'
#### main effect of Food Wanting (model A, con 01, 03, 05)
#m2 = '/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/no_confound/allsubs/neurosynthraw/main/modelA/con_0001/swe_tfce_c01.nii'
#m3 = '/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/no_confound/allsubs/neurosynthraw/main/modelA/con_0003/swe_tfce_c01.nii'
#m4 = '/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/no_confound/allsubs/neurosynthraw/main/modelA/con_0005/swe_tfce_c01.nii'
#### peak voxel spheres reward and hypothalamus network
ns_peak = '/data/pt_02020/Software/ROI_analysis/neurosynth_peak/reward_hypothalamus_merged_spheres_only.nii'

display=plotting.plot_glass_brain(m1, 
                                  display_mode='lyrz', colorbar=False, figure=None, axes=None, 
                                  title='individual values extracted', 
                                  threshold=180, annotate=True, black_bg=False, cmap='plasma', 
                                  alpha=0.7, vmin=None, vmax=None, plot_abs=True, symmetric_cbar='auto', 
                                  resampling_interpolation='continuous')
display.add_overlay(m1_deact, cmap='Blues', threshold=180) 
#display.add_overlay(m2, cmap=plotting.cm.purple_green, threshold=0) 
#display.add_overlay(m3, cmap=plotting.cm.purple_green, threshold=0) 
#display.add_overlay(m4, cmap=plotting.cm.purple_green, threshold=0) 
display.savefig([dir + '4_plots/fmri_values_extracted.pdf'])

#display=plotting.plot_roi(m1, bg_img=avg152T1, cut_coords=None, output_file=None, display_mode='ortho', figure=None, axes=None, title=None, annotate=True, draw_cross=True, black_bg='auto', threshold=0.5, alpha=0.7, cmap=plotting.cm.purple_green, dim='auto', vmin=None, vmax=None, resampling_interpolation='nearest', view_type='continuous', linewidths=2.5)

# ### deactivation: specific intervention effects
# f1='/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/no_confound/allsubs/neurosynthraw/interaction/modelB1/con_0002/swe_tfce_c02.nii' 
# f2='/data/pt_02020/MRI_data/fmri_wanting_results/2nd_level/no_confound/allsubs/neurosynthraw/interaction/modelB1/con_0002/swe_tfce_lpFWE-WB_c02.nii'
# title = 'interaction_modelB1_con_0002_deact'
# display=plotting.plot_glass_brain(f1, display_mode='lyrz', colorbar=True, figure=None, axes=None, title=title, threshold=50, annotate=True, black_bg=False, cmap='Blues', alpha=0.7, vmin=None, vmax=None, plot_abs=True, symmetric_cbar='auto', resampling_interpolation='continuous')
# display.add_contours(f2, threshold=1.3,levels=[1.3], colors='r', filled=False)
# filename = 'no_confound_allsubs_neurosynthraw_interaction_modelB1_con_0002_swe_tfce_c02'
# display.savefig('/data/pt_02020/MRI_data/fmri_wanting_results/4_plots/%s.pdf' %(filename))


# #### for quick check (in terminal)
# ## plotting.show()
                            
                        
