%Script for analysing the gutbrain task and writing onsets into text file
%for SPM

fname='/data/dt_transfer/medawar2beyer/33690.1a_20190110_131113.PRISMA/logfiles/Veronica_Pilot-Wanting4.1_reformatted.log'
logfile=importPresentationLog(fname);

%###########First part##################
%extract onsets of picture presentation and manual selection, and value of
%liking

%time units in presentation = 0.1 ms = 0.0001 s

onsets_pics=[];
onsets_response=[];
onsets_manual=[];
val_response=[];


j=1; %looping through the individual events/lines of the logfile

while 1
      if logfile(j).trial > 5 %only use trials after the start phase
          logfile(j).trial
          if  strcmp(logfile(j).event_type,'Quit')
              %to stop the looping at the end
              break
          end
          
          if strcmp(logfile(j).event_type,'Picture')
              if (sum(logfile(j).code(1:3)~='ITI'))
                  sprintf('identified picture onset %s\n', logfile(j).code)
                  onsets_pics(end+1)=(logfile(j).time-logfile(1).time); %to correct for start of scanning sequence
              end
          end
          
          if strcmp(logfile(j).event_type,'Response')
              onsets_response(end+1)=(logfile(j).time-logfile(1).time);
          end
          
          if strcmp(logfile(j).event_type,'Manual')
              %need to take care that the first manual input is not considered?
              %(before first picture is shown)
              onsets_manual(end+1)=(logfile(j).time-logfile(1).time);;
              if strcmp(logfile(j).code,'missed')
                  val_response(end+1)=999;
              else
                  val_response(end+1)=(str2num(logfile(j).code));
              end
          end
      end
      
      j=j+1;
end

%save onsets into text file for SPM
fileID = fopen('pic.ons','w');
fprintf(fileID,'%.3f\n',onsets_pics*0.0001);
fclose(fileID);

fileID = fopen('manual.ons','w');
fprintf(fileID,'%.3f\n',onsets_manual*0.0001);
fclose(fileID);



