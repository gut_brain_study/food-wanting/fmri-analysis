%-----------------------------------------------------------------------
% Job saved on 10-Jan-2019 12:37:50 by cfg_util (rev $Rev: 6942 $)
% spm SPM - SPM12 (7219)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
dataDir='/data/dt_transfer/thieleking2beyer/'
picDir=([dataDir,'33690.1a_20190110_131113.PRISMA/logfiles/'])
food_ons=dlmread([picDir,'pic_food.ons']);
nonfood_ons=dlmread([picDir,'pic_nonfood.ons']);
food_val=dlmread([picDir,'value_food.txt']);
nonfood_val=dlmread([picDir,'value_nonfood.txt']);

matlabbatch{1}.spm.stats.fmri_spec.dir = {'/data/pt_life/data_fbeyer/gut_brain/analysis_with_modulation'};
matlabbatch{1}.spm.stats.fmri_spec.timing.units = 'secs';
matlabbatch{1}.spm.stats.fmri_spec.timing.RT = 2;
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t = 16;
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0 = 8;
matlabbatch{1}.spm.stats.fmri_spec.sess.scans = {'/data/dt_transfer/thieleking2beyer/33690.1a_20190110_131113.PRISMA/NIFTI/swaus009a001.nii'};
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(1).name = 'food';
%%
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(1).onset = food_ons;
%%
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(1).duration = 0;
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(1).tmod = 0;
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(1).orth = 1;
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(1).pmod.name = 'value';
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(1).pmod.param = food_val;
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(1).pmod.poly = 1;
%%
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(2).name = 'manual selection';
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(2).onset = nonfood_ons;
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(2).duration = 0;
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(2).tmod = 0;
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(2).orth = 1;
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(2).pmod.name = 'value';
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(2).pmod.param = nonfood_val;
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(2).pmod.poly = 1;

matlabbatch{1}.spm.stats.fmri_spec.sess.multi = {''};
matlabbatch{1}.spm.stats.fmri_spec.sess.regress = struct('name', {}, 'val', {});
matlabbatch{1}.spm.stats.fmri_spec.sess.multi_reg = {''};
matlabbatch{1}.spm.stats.fmri_spec.sess.hpf = 128;
matlabbatch{1}.spm.stats.fmri_spec.fact = struct('name', {}, 'levels', {});
matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = [0 0];
matlabbatch{1}.spm.stats.fmri_spec.volt = 1;
matlabbatch{1}.spm.stats.fmri_spec.global = 'None';
matlabbatch{1}.spm.stats.fmri_spec.mthresh = 0.8;
matlabbatch{1}.spm.stats.fmri_spec.mask = {''};
matlabbatch{1}.spm.stats.fmri_spec.cvi = 'AR(1)';
matlabbatch{2}.spm.stats.fmri_est.spmmat(1) = cfg_dep('fMRI model specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{2}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{2}.spm.stats.fmri_est.method.Classical = 1;
matlabbatch{3}.spm.stats.con.spmmat(1) = cfg_dep('Model estimation: SPM.mat File', substruct('.','val', '{}',{2}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{3}.spm.stats.con.consess{1}.tcon.name = 'manual > picture';
matlabbatch{3}.spm.stats.con.consess{1}.tcon.weights = [-1 1];
matlabbatch{3}.spm.stats.con.consess{1}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{2}.tcon.name = 'picture viewing';
matlabbatch{3}.spm.stats.con.consess{2}.tcon.weights = [1 0];
matlabbatch{3}.spm.stats.con.consess{2}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.consess{3}.tcon.name = 'manual selection';
matlabbatch{3}.spm.stats.con.consess{3}.tcon.weights = [0 1];
matlabbatch{3}.spm.stats.con.consess{3}.tcon.sessrep = 'none';
matlabbatch{3}.spm.stats.con.delete = 0;

spm_jobman('run',matlabbatch);
	%spm_jobman('run', 'interactive','matlabbatch');
	clear matlabbatch;
	disp('Done.');