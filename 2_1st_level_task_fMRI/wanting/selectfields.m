function out = selectfields(inp,req,noerr ) 
% selectfields  Select fields from struct with optional noerror 
%  2017-07-09  Matlab2006+  Copyright (c) 2017, W J Whiten  BSD License
%  2019-03-12  Updated for struct arrays, thanks Jos, Stephen
%  2019-03-13  Code replaced by tidier code from Stephen Cobeldick & Jos
%
% s2=selectfields(s1,names,noerror)
%  inp      Input struct or cell array of pairs
%  req      Cell array of field names to be copied
%  noerr    If present no error message if any name not a struct field
%
%  out      Struct giving selected fields from struct1
%
% Examples:
% t1=selectfields(struct('a',[1,2,3],'b',2,'c',3),{'a','c'})
% t1 = 
%   struct with fields:
% 
%     a: [1 2 3]
%     c: 3
%
% selectfields(struct('a',1,'c',3,'aa',11),{'a','b','c'},true)
% ans = 
%   struct with fields:
% 
%     a: 1
%     c: 3
if(~isstruct(inp))
    error('selectfields: first input must be a struct')
end
% check for invalid req names
idx = ~isfield(inp,req ); 
if( (nargin<3||~noerr) && any(idx) )
    str = sprintf(' %s,',req{idx }); 
    error('selectfields: Fieldnames not in structure:%s',str ) 
end
% remove fields not needed
out = rmfield(inp,setdiff(fieldnames(inp),req )); 
end
% t1=selectfields(struct('a',[1,2,3],'b',2,'c',3),{'a','c'})
% t1 = 
%   struct with fields:
% 
%     a: [1 2 3]
%     c: 3
% selectfields(struct(),{'a','b','c'},true)
% selectfields(struct('a',1,'c',3,'aa',11),{'a','b','c'})
% ans = 
%   struct with no fields.
% Error using selectfields (line 38)
% selectfields: Fieldnames not in structure: b, 