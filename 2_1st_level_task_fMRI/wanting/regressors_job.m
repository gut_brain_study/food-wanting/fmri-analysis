%-----------------------------------------------------------------------
% Job saved on 09-Apr-2021 12:33:37 by cfg_util (rev $Rev: 6942 $)
% spm SPM - SPM12 (7219)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
matlabbatch{1}.spm.stats.fmri_spec.dir = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.timing.units = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.timing.RT = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t = 16;
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0 = 8;
matlabbatch{1}.spm.stats.fmri_spec.sess.scans = '<UNDEFINED>';

matlabbatch{1}.spm.stats.fmri_spec.sess.cond(1).name = 'F';
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(1).onset = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(1).duration = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(1).tmod = 0;

matlabbatch{1}.spm.stats.fmri_spec.sess.cond(1).pmod(1).name = 'wanting';
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(1).pmod(1).param = [0 NaN 1 3 2];
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(1).pmod(1).poly = '<UNDEFINED>';

matlabbatch{1}.spm.stats.fmri_spec.sess.cond(1).pmod(2).name = 'kcal';
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(1).pmod(2).param = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(1).pmod(2).poly = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(1).orth = 1;



matlabbatch{1}.spm.stats.fmri_spec.sess.cond(2).name = 'NF';
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(2).onset = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(2).duration = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(2).tmod = 0;
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(2).pmod.name = 'wanting';
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(2).pmod.param = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(2).pmod.poly = '<UNDEFINED>';
matlabbatch{1}.spm.stats.fmri_spec.sess.cond(2).orth = 1;

matlabbatch{1}.spm.stats.fmri_spec.sess.multi = {''};
matlabbatch{1}.spm.stats.fmri_spec.sess.regress = struct('name', {}, 'val', {});
matlabbatch{1}.spm.stats.fmri_spec.sess.multi_reg = {''};
matlabbatch{1}.spm.stats.fmri_spec.sess.hpf = 128;
matlabbatch{1}.spm.stats.fmri_spec.fact = struct('name', {}, 'levels', {});
matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = [0 0];
matlabbatch{1}.spm.stats.fmri_spec.volt = 1;
matlabbatch{1}.spm.stats.fmri_spec.global = 'None';
matlabbatch{1}.spm.stats.fmri_spec.mthresh = 0.8;
matlabbatch{1}.spm.stats.fmri_spec.mask = {''};
matlabbatch{1}.spm.stats.fmri_spec.cvi = 'AR(1)';
