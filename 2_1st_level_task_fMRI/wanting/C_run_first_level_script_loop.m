% List of open inputs
nrun = 1; % enter the number of runs here
jobfile = {'/data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/2_1st_level_task_fMRI/wanting/C_first_level_script_loop.m'};
jobs = repmat(jobfile, 1, nrun);
inputs = cell(0, nrun);
for crun = 1:nrun
end
spm('defaults', 'FMRI');
spm_jobman('run', jobs, inputs{:});
