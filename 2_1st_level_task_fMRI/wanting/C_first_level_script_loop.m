%-----------------------------------------------------------------------
% Job saved on 22-Mar-2021 17:48:53 by cfg_util (rev $Rev: 6942 $)
% spm SPM - SPM12 (7219)
% cfg_basicio BasicIO - Unknown
% swe SwE - Unknown
% medawar 2021-03-23 for GUT-BRAIN Food Wanting analysis
%-----------------------------------------------------------------------
clc; clear all

% initialise SPM defaults --> here?!
spm('defaults', 'FMRI');
spm_jobman('initcfg');
% 
% % input dir for nii files
dataDir = '/data/pt_02020/MRI_data/fmriprep/fmriprep/';

% % % output dir Model C1 (liking)
% outdir = '/data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelC1/';
% % % output dir Model C2 (liking)
outdir = '/data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelC2/';

subjList = [01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61];
sesList = [01 02 03 04];

% subj = 04
% ses = 04

form='sub-%s ses-%s\n'; 

% loop over subjects + timepoints
for subj = subjList(1):subjList(end) %subjList(26):subjList(end) %%% adapt if only for specific subjects

    %% exclude subj with all missing liking ratings (according to >10% missed items or missed session/visit)
    if subj == 03 | subj == 07 | subj == 11 | subj == 19 | subj == 22 | subj == 30 | subj == 31 | subj == 32 | subj == 34 | subj == 38 | subj == 41 | subj == 45 | subj == 46 |subj == 52| subj == 54 | subj == 60 | subj == 61
            continue
    end
    
    for ses = 1:numel(sesList) %ses

    subj = num2str(subj, '%02d'); % zero-pads variables to be 2 characters long
    ses = num2str(ses, '%02d'); %% change back to j 

    %% skip these sessions due to 1 for all art.pics
    if subj == "04" && ses == "01" || subj == "04" && ses == "02"
            continue
    end

    fprintf(form,subj,ses)



%% check if session is missing, then skip
if exist([dataDir 'sub-' subj '/ses-' ses '/func/']) == 0
    continue 
end
   

%% change back to analysis directory
cd('/data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/2_1st_level_task_fMRI/wanting');


%% define nii input (smoothed FWHM 6mm fmriprep output)
input_nii=([dataDir 'sub-' subj '/ses-' ses '/func/ssub-' subj '_ses-' ses '_task-wanting_space-MNI152NLin2009cAsym_desc-preproc_bold.nii']);


%% create vectors for onsets
% target file: /data/pt_02020/MRI_data/fmriprep/fmriprep/sub-01/ses-01/func/sub-01_ses-01_task-wanting_events.tsv
logfile_tmp = tsv_read([dataDir 'sub-' subj '/ses-' ses '/func/sub-' subj '_ses-' ses '_task-wanting_events.tsv'], '',true);
vars = fieldnames(logfile_tmp);
vars_new = erase(vars,"x0x22");
vars_new = erase(vars_new,"0x22");
logfile = cell2struct(struct2cell(logfile_tmp), vars_new);


%% make cell arrays for specific onsets (onset arrays and reward rating arrays)
% onsets_pictures = [];
% onsets_buttonpress = [];
onsets_food_pictures = []; 
onsets_art_pictures = [];
reward_food = [];
reward_art = [];
% reward_pictures = [];

liking_food = [];
liking_art = [];
liking = [];

% reward_food_demeaned = [];
% reward_art_demeaned = [];
% liking_food_demeaned = [];
% liking_art_demeaned = [];

missed = [];
missed_food = [];
missed_art = [];


% replace NA as string to enable strcmp
for ii = 1:size(logfile.wanting,1)
    if logfile.wanting{ii,1} == "NA"
       logfile.wanting{ii,1}='''NA''';
    else
    end
    if logfile.liking{ii,1} == "NA"
       logfile.liking{ii,1}='''NA''';
    else
    end
end


%% take for separate onset regressors
for i = 1:size(logfile.stim_type,1)
    % for all Pictures with valid wanting rating add to regressors
    if strcmp(logfile.trial_type{i,1}(2:end-1),'Picture')
        liking(size(liking,2)+1) = str2double(string(logfile.liking(i)));
    end
    if strcmp(logfile.trial_type{i,1}(2:end-1),'Picture') && strcmp(logfile.stim_category{i,1}(2:end-1),'F') && strcmp(logfile.wanting{i,1}(2:end-1), 'NA') | strcmp(logfile.liking{i,1}(2:end-1), 'NA')
       missed_food(end+1) = [1]; %num2cell(1);
       
       % for all Pictures with wanting rating NA add to missed regressor
    elseif strcmp(logfile.trial_type{i,1}(2:end-1),'Picture') && strcmp(logfile.stim_category{i,1}(2:end-1),'F')
            onsets_food_pictures(size(onsets_food_pictures,2)+1) = logfile.onset(i);
            reward_food(size(reward_food,2)+1) = str2double(string(logfile.wanting(i)));
            liking_food(size(liking_food,2)+1) = str2double(string(logfile.liking(i)));
            missed_food(end+1) = [0]; %num2cell(1);
    end
    if strcmp(logfile.trial_type{i,1}(2:end-1),'Picture') && strcmp(logfile.stim_category{i,1}(2:end-1),'NF') && strcmp(logfile.wanting{i,1}(2:end-1), 'NA') | strcmp(logfile.liking{i,1}(2:end-1), 'NA')
       missed_art(end+1) = [1]; %num2cell(1);
      
    elseif strcmp(logfile.trial_type{i,1}(2:end-1),'Picture') && strcmp(logfile.stim_category{i,1}(2:end-1),'NF')
        % for all Pictures with wanting rating NA add to missed regressor
            onsets_art_pictures(size(onsets_art_pictures,2)+1) = logfile.onset(i);
            reward_art(size(reward_art,2)+1) = str2double(string(logfile.wanting(i)));
            liking_art(size(liking_art,2)+1) = str2double(string(logfile.liking(i)));

            % add 0 to missing value 
            missed_art(end+1) = [0]; %num2cell(1);
    end
end

% %% compute demeaned variables for models with >1 param. modulator
% for i = 1:size(logfile.stim_type,1)
%     if strcmp(logfile.trial_type{i,1}(2:end-1),'Picture') && strcmp(logfile.stim_category{i,1}(2:end-1),'F') 
%     reward_food_demeaned(size(reward_food_demeaned,2)+1)= str2double(string(logfile.wanting(i))) - mean(reward_food);
%     liking_food_demeaned(size(liking_food_demeaned,2)+1)= str2double(string(logfile.liking(i))) - mean(liking_food);
%     end
%     if strcmp(logfile.trial_type{i,1}(2:end-1),'Picture') && strcmp(logfile.stim_category{i,1}(2:end-1),'NF') 
%     reward_art_demeaned(size(reward_art_demeaned,2)+1)= str2double(string(logfile.wanting(i))) - mean(reward_art);
%     liking_art_demeaned(size(liking_art_demeaned,2)+1)= str2double(string(logfile.liking(i))) - mean(liking_art);
%     end
% end


%% load motion params (6 rigid-body motion parameters and the binary TR outlier regressor (>0.9mm))
tParams.motion_regress ='yes'; % use  motion  parameters  as  regressors? yes/no

% 6 rigid-body motion params - fmriprep output
% target file: /data/pt_02020/MRI_data/fmriprep/fmriprep/sub-01/ses-01/func/sub-01_ses-01_task-wanting_desc-confounds_regressors.tsv
motion_reg_tmp = tsv_read([dataDir 'sub-' subj '/ses-' ses '/func/sub-' subj '_ses-' ses '_task-wanting_desc-confounds_regressors.tsv'],'',true);

% to do select columns of interest (trans x/y/z and rot x/y/z)
motion_reg=selectfields(motion_reg_tmp,{'trans_x', 'trans_y', 'trans_z','rot_x','rot_y','rot_z'}) 


% % load TR outlier regressor
% % target file for binary regressor: /data/pt_02020/MRI_data/fmriprep/fmriprep/sub-01/ses-03/func/sub-01_ses-03_task-wanting_movement-confounds.tsv

% % note some subjects don't have files if all are 0
% if session is missing, then skip
if exist([dataDir 'sub-' subj '/ses-' ses '/func/sub-' subj '_ses-' ses '_task-wanting_movement-confounds.tsv'])
   % input format: lines = 950 TRs; columns = n-according to no. of outlier TRs        
    motion_reg_binary = load([dataDir 'sub-' subj '/ses-' ses '/func/sub-' subj '_ses-' ses '_task-wanting_movement-confounds.tsv'])
   % append binary file to 6 motion param
   motion_reg.TR09 = motion_reg_binary;
end

%% to do: loop for model C1 and model C2
% model = [1 2];
% for model = 1:length(model)
%     
% end

% make subject-session-specific outdir
mkdir([outdir 'sub-' subj '/ses-' ses]);
wd = ([outdir 'sub-' subj '/ses-' ses]);

%% change to output dir per sub per ses
cd([outdir 'sub-' subj '/ses-' ses]);

% % load into matlab as regressor
writetable(struct2table(motion_reg), 'motion_reg.txt','WriteVariableNames',false)

%%% define dir and load input files
matlabbatch{1}.spm.stats.fmri_spec.dir = cellstr(wd);
matlabbatch{1}.spm.stats.fmri_spec.timing.units = 'secs';
matlabbatch{1}.spm.stats.fmri_spec.timing.RT = 2; %% interscan interval (TR)
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t = 16; % microtime resolution or number of slices
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0 = 8; % microtime onset
matlabbatch{1}.spm.stats.fmri_spec.sess(1).scans = cellstr(input_nii);


%% load onsets of picture stimuli

%% load onsets for parametric modulations (Model A - food + non-food + wanting)
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).name = 'pic food';
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).onset = [onsets_food_pictures];
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).duration = 4;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).tmod = 0;

matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(1).name = 'wanting_food';
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(1).param = [liking_food]; %% for model C1: [reward_food] %% for model C2: [liking_food];
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(1).poly = 1;

matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).orth = 1; %orthogonalization: for model C1 put to 0, for C2 put to 1;

% % %% Model C1: with liking as parametric modulator of picture onset
% matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(2).name = 'liking food';
% matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(2).param = [liking_food];
% matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(2).poly = 1;


matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).name = 'pic non-food';
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).onset = [onsets_art_pictures];
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).duration = 4;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).tmod = 0;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).orth = 1; %% to check: for model C1 put to 0, for C2 put to 1;

matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).pmod(1).name = 'wanting_nonfood';
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).pmod(1).param = [liking_art];%% for model C1 [reward_art]; %% for model C2 [liking_art]; 
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).pmod(1).poly = 1;

% % % %% Model C1: with liking as parametric modulator of picture onset
% matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).pmod(2).name = 'liking art';
% matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).pmod(2).param = [liking_art];
% matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).pmod(2).poly = 1;
%                                                    

%%% add missed item regressors
% matlabbatch{1}.spm.stats.fmri_spec.sess(1).regress(1).name = 'missed_food';
% matlabbatch{1}.spm.stats.fmri_spec.sess(1).regress(1).val = [missed_food];
% matlabbatch{1}.spm.stats.fmri_spec.sess(1).regress(2).name = 'missed_art';
% matlabbatch{1}.spm.stats.fmri_spec.sess(1).regress(2).val = [missed_art];


%%
matlabbatch{1}.spm.stats.fmri_spec.sess(1).multi = {''};
matlabbatch{1}.spm.stats.fmri_spec.sess(1).regress = struct('name', {}, 'val', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(1).multi_reg = {'motion_reg.txt'}; %% 6 motion params
matlabbatch{1}.spm.stats.fmri_spec.sess(1).hpf = 128;

%% Model params
matlabbatch{1}.spm.stats.fmri_spec.fact = struct('name', {}, 'levels', {});
matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = [0 0];
matlabbatch{1}.spm.stats.fmri_spec.volt = 1;
matlabbatch{1}.spm.stats.fmri_spec.global = 'None';
matlabbatch{1}.spm.stats.fmri_spec.mthresh = 0.4; %%% default was 0.8
matlabbatch{1}.spm.stats.fmri_spec.mask = {''};
matlabbatch{1}.spm.stats.fmri_spec.cvi = 'AR(1)';


%% Model specification
matlabbatch{2}.spm.stats.fmri_est.spmmat(1) = cfg_dep('fMRI model specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{2}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{2}.spm.stats.fmri_est.method.Classical = 1;

%% Model estimation and setting of contrasts
%%% note: the onsets order: 
% % % onsets_food_pictures
% % % reward_food
% % % onsets_art_pictures
% % % reward_art
% % % liking
% % % last column: average
matlabbatch{3}.spm.stats.con.spmmat(1) = cfg_dep('Model estimation: SPM.mat File', substruct('.','val', '{}',{2}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));

% %%% for model C1 (liking as param mod. of NO interest):
% matlabbatch{3}.spm.stats.con.consess{1}.tcon.name = 'food > art';
% matlabbatch{3}.spm.stats.con.consess{1}.tcon.weights = [1 0  0 -1 0 0 0];
% matlabbatch{3}.spm.stats.con.consess{1}.tcon.sessrep = 'repl';
% matlabbatch{3}.spm.stats.con.consess{2}.tcon.name = 'art > food';
% matlabbatch{3}.spm.stats.con.consess{2}.tcon.weights = [-1 0 0 1 0 0 0];
% matlabbatch{3}.spm.stats.con.consess{2}.tcon.sessrep = 'repl';
% matlabbatch{3}.spm.stats.con.consess{3}.tcon.name = 'food > art wanting slope';
% matlabbatch{3}.spm.stats.con.consess{3}.tcon.weights = [0 1 0 0 -1 0 0];
% matlabbatch{3}.spm.stats.con.consess{3}.tcon.sessrep = 'repl';
% matlabbatch{3}.spm.stats.con.consess{4}.tcon.name = 'art > food wanting slope';
% matlabbatch{3}.spm.stats.con.consess{4}.tcon.weights = [0 -1 0 0 1 0 0];
% matlabbatch{3}.spm.stats.con.consess{4}.tcon.sessrep = 'repl';
% matlabbatch{3}.spm.stats.con.consess{5}.tcon.name = 'wanting modulation';
% matlabbatch{3}.spm.stats.con.consess{5}.tcon.weights = [0 0.5 0 0 0.5 0 0];
% matlabbatch{3}.spm.stats.con.consess{5}.tcon.sessrep = 'repl';
% matlabbatch{3}.spm.stats.con.delete = 0;

%%%% for model C2 (liking as param mod. of interest)
matlabbatch{3}.spm.stats.con.consess{1}.tcon.name = 'food > art liking slope';
matlabbatch{3}.spm.stats.con.consess{1}.tcon.weights = [0 1 0 -1 0];
matlabbatch{3}.spm.stats.con.consess{1}.tcon.sessrep = 'repl';
matlabbatch{3}.spm.stats.con.consess{2}.tcon.name = 'art > food liking slope';
matlabbatch{3}.spm.stats.con.consess{2}.tcon.weights = [0 -1 0 1 0];
matlabbatch{3}.spm.stats.con.consess{2}.tcon.sessrep = 'repl';
matlabbatch{3}.spm.stats.con.consess{3}.tcon.name = 'liking modulation';
matlabbatch{3}.spm.stats.con.consess{3}.tcon.weights = [0 0.5 0 0.5 0];
matlabbatch{3}.spm.stats.con.consess{3}.tcon.sessrep = 'repl';
matlabbatch{3}.spm.stats.con.delete = 0;

%%%% here?!?!?! apparently yes.
spm_jobman('run', matlabbatch);

   end
end