% List of open inputs
% fMRI model specification: Directory - cfg_files
% fMRI model specification: Units for design - cfg_menu
% fMRI model specification: Interscan interval - cfg_entry
% fMRI model specification: Scans - cfg_files
% fMRI model specification: Onsets - cfg_entry
% fMRI model specification: Durations - cfg_entry
% fMRI model specification: Values - cfg_entry
% fMRI model specification: Polynomial Expansion - cfg_menu
% fMRI model specification: Values - cfg_entry
% fMRI model specification: Polynomial Expansion - cfg_menu
% fMRI model specification: Onsets - cfg_entry
% fMRI model specification: Durations - cfg_entry
% fMRI model specification: Values - cfg_entry
% fMRI model specification: Polynomial Expansion - cfg_menu
nrun = X; % enter the number of runs here
jobfile = {'/data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/2_1st_level_task_fMRI/wanting/regressors_job.m'};
jobs = repmat(jobfile, 1, nrun);
inputs = cell(14, nrun);
for crun = 1:nrun
    inputs{1, crun} = MATLAB_CODE_TO_FILL_INPUT; % fMRI model specification: Directory - cfg_files
    inputs{2, crun} = MATLAB_CODE_TO_FILL_INPUT; % fMRI model specification: Units for design - cfg_menu
    inputs{3, crun} = MATLAB_CODE_TO_FILL_INPUT; % fMRI model specification: Interscan interval - cfg_entry
    inputs{4, crun} = MATLAB_CODE_TO_FILL_INPUT; % fMRI model specification: Scans - cfg_files
    inputs{5, crun} = MATLAB_CODE_TO_FILL_INPUT; % fMRI model specification: Onsets - cfg_entry
    inputs{6, crun} = MATLAB_CODE_TO_FILL_INPUT; % fMRI model specification: Durations - cfg_entry
    inputs{7, crun} = MATLAB_CODE_TO_FILL_INPUT; % fMRI model specification: Values - cfg_entry
    inputs{8, crun} = MATLAB_CODE_TO_FILL_INPUT; % fMRI model specification: Polynomial Expansion - cfg_menu
    inputs{9, crun} = MATLAB_CODE_TO_FILL_INPUT; % fMRI model specification: Values - cfg_entry
    inputs{10, crun} = MATLAB_CODE_TO_FILL_INPUT; % fMRI model specification: Polynomial Expansion - cfg_menu
    inputs{11, crun} = MATLAB_CODE_TO_FILL_INPUT; % fMRI model specification: Onsets - cfg_entry
    inputs{12, crun} = MATLAB_CODE_TO_FILL_INPUT; % fMRI model specification: Durations - cfg_entry
    inputs{13, crun} = MATLAB_CODE_TO_FILL_INPUT; % fMRI model specification: Values - cfg_entry
    inputs{14, crun} = MATLAB_CODE_TO_FILL_INPUT; % fMRI model specification: Polynomial Expansion - cfg_menu
end
spm('defaults', 'FMRI');
spm_jobman('run', jobs, inputs{:});
