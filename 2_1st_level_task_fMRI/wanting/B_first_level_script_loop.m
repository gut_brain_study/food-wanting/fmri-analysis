%-----------------------------------------------------------------------
% Job saved on 22-Mar-2021 17:48:53 by cfg_util (rev $Rev: 6942 $)
% spm SPM - SPM12 (7219)
% cfg_basicio BasicIO - Unknown
% swe SwE - Unknown
% medawar 2021-03-23 for GUT-BRAIN Food Wanting analysis
%-----------------------------------------------------------------------
clc; clear all

% initialise SPM defaults --> here?!
spm('defaults', 'FMRI');
spm_jobman('initcfg');
% 
% % input dir for nii files
dataDir = '/data/pt_02020/MRI_data/fmriprep/fmriprep/';

% % output dir Model B1 (kcal*wanting)
% outdir = '/data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelB1/';
% % output dir Model B2 (fiber*wanting)
outdir = '/data/pt_02020/MRI_data/fmri_wanting_results/1st_level/modelB2/';


subjList = [01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61];
% subjList = [01 02 03 04 05];
sesList = [01 02 03 04];

subj = 01
%ses = 01

form='sub-%s ses-%s\n'; 

% loop over subjects + timepoints
for subj = subjList(1):subjList(end) %% or subjList(30):subjList(30) %subj 
    for ses = 1:numel(sesList) %ses

    subj = num2str(subj, '%02d'); % zero-pads variables to be 2 characters long
    ses = num2str(ses, '%02d'); %% change back to j 
    
    %% skip subject due to a posteriori detected depression/ neurological disorder
%     if subj == "30"
%             continue
%     end
%     
    if subj == "34"
            continue
    end
    
    %% skip these sessions due to 1 for all art.pics
    if subj == "04" && ses == "01" || subj == "04" && ses == "02"
            continue
    end

    fprintf(form,subj,ses)


%% check if session is missing, then skip
if exist([dataDir 'sub-' subj '/ses-' ses '/func/']) == 0
    continue 
end
   

%% change back to analysis directory
cd('/data/pt_02020/MRI_data/scripts/1_FUNCTIONAL/2_1st_level_task_fMRI/wanting');


%% define nii input (smoothed FWHM 6mm fmriprep output)
input_nii=([dataDir 'sub-' subj '/ses-' ses '/func/ssub-' subj '_ses-' ses '_task-wanting_space-MNI152NLin2009cAsym_desc-preproc_bold.nii']);


%% create vectors for onsets
% target file: /data/pt_02020/MRI_data/fmriprep/fmriprep/sub-01/ses-01/func/sub-01_ses-01_task-wanting_events.tsv
logfile_tmp = tsv_read([dataDir 'sub-' subj '/ses-' ses '/func/sub-' subj '_ses-' ses '_task-wanting_events.tsv'], '',true);
vars = fieldnames(logfile_tmp);
vars_new = erase(vars,"x0x22");
vars_new = erase(vars_new,"0x22");
logfile = cell2struct(struct2cell(logfile_tmp), vars_new);


%% make cell arrays for specific onsets (onset arrays and reward rating arrays)
onsets_food_pictures = []; 
onsets_art_pictures = [];
reward_food = [];
reward_art = [];
kcal_100g = [];
fiber_100g = [];
wanting_x_kcal = [];
wanting_x_fiber = [];
missed_food = [];
missed_art = [];

% replace NA as string to enable strcmp
for ii = 1:size(logfile.wanting,1)
    if logfile.wanting{ii,1} == "NA"
       logfile.wanting{ii,1}='''NA''';
    else
    end
end

for i = 1:size(logfile.stim_type,1)
    if strcmp(logfile.trial_type{i,1}(2:end-1),'Picture') && strcmp(logfile.stim_category{i,1}(2:end-1),'F') && strcmp(logfile.wanting{i,1}(2:end-1), 'NA')
        missed_food(end+1) = [1];
       
    elseif strcmp(logfile.trial_type{i,1}(2:end-1),'Picture') && strcmp(logfile.stim_category{i,1}(2:end-1),'F')
           onsets_food_pictures(size(onsets_food_pictures,2)+1) = logfile.onset(i);
           reward_food(size(reward_food,2)+1) = str2double(string(logfile.wanting(i)));
           kcal_100g(size(kcal_100g,2)+1)= string(logfile.kcal_100g(i)); 
           fiber_100g(size(fiber_100g,2)+1)= string(logfile.fiber_100g(i));
           wanting_x_kcal(size(wanting_x_kcal,2)+1)= str2num(string(logfile.wanting(i))).*str2num(string(logfile.kcal_100g(i)));
           wanting_x_fiber(size(wanting_x_fiber,2)+1)= str2num(string(logfile.wanting(i))).*str2num(string(logfile.fiber_100g(i)));
           missed_food(end+1) = [0];
    end
    if strcmp(logfile.trial_type{i,1}(2:end-1),'Picture') && strcmp(logfile.stim_category{i,1}(2:end-1),'NF') && strcmp(logfile.wanting{i,1}(2:end-1), 'NA')
        missed_art(end+1) = [1]; 
        
    elseif strcmp(logfile.trial_type{i,1}(2:end-1),'Picture') && strcmp(logfile.stim_category{i,1}(2:end-1),'NF') 
           onsets_art_pictures(size(onsets_art_pictures,2)+1) = logfile.onset(i);
           reward_art(size(reward_art,2)+1) = str2double(string(logfile.wanting(i)));
           missed_art(end+1) = [0]; 
    end
end

%% log-transform fiber_100g (due to skewness)
skewness(fiber_100g)
fiber_100g = log10(fiber_100g+1);
wanting_x_fiber = log10(wanting_x_fiber+1);


% %% compute demeaned variables for models with >1 param. modulator
% for i = 1:size(logfile.stim_type,1)
%     if strcmp(logfile.trial_type{i,1}(2:end-1),'Picture') && strcmp(logfile.stim_category{i,1}(2:end-1),'F') 
%     reward_food_demeaned(size(reward_food_demeaned,2)+1)= str2double(string(logfile.wanting(i))) - mean(reward_food);
%     kcal_100g_demeaned(size(kcal_100g_demeaned,2)+1)= str2double(string(logfile.kcal_100g(i))) - mean(kcal_100g);
%     fiber_100g_demeaned(size(fiber_100g_demeaned,2)+1)= str2double(string(logfile.fiber_100g(i))) - mean(fiber_100g);
%     wanting_x_kcal_demeaned(size(wanting_x_kcal_demeaned,2)+1)= str2double(string(logfile.kcal_100g(i))) - mean(wanting_x_kcal);
%     wanting_x_fiber_demeaned(size(wanting_x_fiber_demeaned,2)+1)= str2double(string(logfile.fiber_100g(i))) - mean(wanting_x_fiber);
%     end
% end



%% load motion params (6 rigid-body motion parameters and the binary TR outlier regressor (>0.9mm))
tParams.motion_regress ='yes'; % use  motion  parameters  as  regressors? yes/no

% 6 rigid-body motion params - fmriprep output
% target file: /data/pt_02020/MRI_data/fmriprep/fmriprep/sub-01/ses-01/func/sub-01_ses-01_task-wanting_desc-confounds_regressors.tsv
motion_reg_tmp = tsv_read([dataDir 'sub-' subj '/ses-' ses '/func/sub-' subj '_ses-' ses '_task-wanting_desc-confounds_regressors.tsv'],'',true);

% to do select columns of interest (trans x/y/z and rot x/y/z)
motion_reg=selectfields(motion_reg_tmp,{'trans_x', 'trans_y', 'trans_z','rot_x','rot_y','rot_z'});


% % load TR outlier regressor
% % target file for binary regressor: /data/pt_02020/MRI_data/fmriprep/fmriprep/sub-01/ses-03/func/sub-01_ses-03_task-wanting_movement-confounds.tsv

% % note some subjects don't have files if all are 0
% if session is missing, then skip
if exist([dataDir 'sub-' subj '/ses-' ses '/func/sub-' subj '_ses-' ses '_task-wanting_movement-confounds.tsv'])
   % input format: lines = 950 TRs; columns = n-according to no. of outlier TRs        
    motion_reg_binary = load([dataDir 'sub-' subj '/ses-' ses '/func/sub-' subj '_ses-' ses '_task-wanting_movement-confounds.tsv'])
   % append binary file to 6 motion param
   motion_reg.TR09 = motion_reg_binary;
end



% make subject-session-specific outdir
mkdir([outdir 'sub-' subj '/ses-' ses]);
wd = ([outdir 'sub-' subj '/ses-' ses]);

%% change to output dir per sub per ses
cd([outdir 'sub-' subj '/ses-' ses]);

% % load into matlab as regressor
writetable(struct2table(motion_reg), 'motion_reg.txt','WriteVariableNames',false)

%%% define dir and load input files
matlabbatch{1}.spm.stats.fmri_spec.dir = cellstr(wd);
matlabbatch{1}.spm.stats.fmri_spec.timing.units = 'secs';
matlabbatch{1}.spm.stats.fmri_spec.timing.RT = 2; %% interscan interval (TR)
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t = 16; % microtime resolution or number of slices
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0 = 8; % microtime onset
matlabbatch{1}.spm.stats.fmri_spec.sess(1).scans = cellstr(input_nii);


%% load onsets of picture stimuli

%% load onsets for parametric modulations (Model A - food + non-food + wanting)
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).name = 'pic food';
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).onset = [onsets_food_pictures];
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).duration = 4;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).tmod = 0;

matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(1).name = 'wanting_food';
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(1).param = [reward_food];
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(1).poly = 1;


% % % %% Model B1: with kcal*wanting interaction
% matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(2).name = 'kcal_100g';
% matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(2).param = [kcal_100g];
% matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(2).poly = 1;
% 
% matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(3).name = 'wanting_kcal';
% matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(3).param = [wanting_x_kcal];
% matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(3).poly = 1;

matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).orth = 0; %orthogonalization OFF here!

% % % % % %% Model B2: with fiber*wanting interaction
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(2).name = 'fiber_100g';
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(2).param = [fiber_100g]; 
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(2).poly = 1;

matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(3).name = 'wanting_fiber';
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(3).param = [wanting_x_fiber]; 
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod(3).poly = 1;


matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).name = 'pic non-food';
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).onset = [onsets_art_pictures];
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).duration = 4;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).tmod = 0;

matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).pmod.name = 'wanting_nonfood';
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).pmod.param = [reward_art];
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).pmod.poly = 1;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).orth = 1;

matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).orth = 1; %orthogonalization ON here! (only 1 param)


% %%% add missed item regressors
% matlabbatch{1}.spm.stats.fmri_spec.sess(1).regress(1).name = 'missed_food';
% matlabbatch{1}.spm.stats.fmri_spec.sess(1).regress(1).val = [missed_food];
% matlabbatch{1}.spm.stats.fmri_spec.sess(1).regress(2).name = 'missed_art';
% matlabbatch{1}.spm.stats.fmri_spec.sess(1).regress(2).val = [missed_art];
% 

%%
matlabbatch{1}.spm.stats.fmri_spec.sess(1).multi = {''};
%matlabbatch{1}.spm.stats.fmri_spec.sess(1).regress = struct('name', {}, 'val', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(1).multi_reg = {'motion_reg.txt'}; %% 6 motion params
matlabbatch{1}.spm.stats.fmri_spec.sess(1).hpf = 128;

%% Model params
matlabbatch{1}.spm.stats.fmri_spec.fact = struct('name', {}, 'levels', {});
matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = [0 0];
matlabbatch{1}.spm.stats.fmri_spec.volt = 1;
matlabbatch{1}.spm.stats.fmri_spec.global = 'None';
matlabbatch{1}.spm.stats.fmri_spec.mthresh = 0.4; %%% implicit masking threshold (default was 0.8) 
matlabbatch{1}.spm.stats.fmri_spec.mask = {''};
matlabbatch{1}.spm.stats.fmri_spec.cvi = 'AR(1)';


%% Model specification
matlabbatch{2}.spm.stats.fmri_est.spmmat(1) = cfg_dep('fMRI model specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{2}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{2}.spm.stats.fmri_est.method.Classical = 1;

%% Model estimation and setting of contrasts
%%% note: the onsets order: 
% % % onsets_food_pictures
% % % reward_food
% % % nutrient (kcal or fiber)
% % % wanting*nutrient interaction
% % % onsets_art_pictures
% % % reward_art
% % % last column: average
matlabbatch{3}.spm.stats.con.spmmat(1) = cfg_dep('Model estimation: SPM.mat File', substruct('.','val', '{}',{2}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));

%%%% for model B1:
% matlabbatch{3}.spm.stats.con.consess{1}.tcon.name = 'wanting*kcal';
% matlabbatch{3}.spm.stats.con.consess{2}.tcon.name = 'kcal';

%%%% for model B2:
matlabbatch{3}.spm.stats.con.consess{1}.tcon.name = 'wanting*fiber';
matlabbatch{3}.spm.stats.con.consess{2}.tcon.name = 'fiber';

matlabbatch{3}.spm.stats.con.consess{1}.tcon.weights = [0 0 0 1 0 0];
matlabbatch{3}.spm.stats.con.consess{1}.tcon.sessrep = 'repl';
matlabbatch{3}.spm.stats.con.consess{2}.tcon.weights = [0 0 1 0 0 0];
matlabbatch{3}.spm.stats.con.consess{2}.tcon.sessrep = 'repl';
matlabbatch{3}.spm.stats.con.delete = 0;

%%%% here?!?!?! apparently yes.
spm_jobman('run', matlabbatch);

   end
end