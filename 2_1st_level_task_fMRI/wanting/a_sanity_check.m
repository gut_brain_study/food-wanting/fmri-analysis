%-----------------------------------------------------------------------
% Job saved on 22-Mar-2021 17:48:53 by cfg_util (rev $Rev: 6942 $)
% spm SPM - SPM12 (7219)
% cfg_basicio BasicIO - Unknown
% swe SwE - Unknown
% medawar 2021-03-23 for GUT-BRAIN Food Wanting analysis
%-----------------------------------------------------------------------
clc; clear all

% initialise SPM defaults --> here?!
spm('defaults', 'FMRI');
spm_jobman('initcfg');

% input dir for nii files
dataDir = '/data/pt_02020/MRI_data/fmriprep/fmriprep/';
% input dir for logfiles (.tsv format)
dataDir_behav = '/data/pt_02020/MRI_data/BIDS/';
% output dir
outdir = '/data/pt_02020/MRI_data/fmri_wanting_results/1st_level/sanitycheck/';

%subjList = [01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 56 57 58 59 60 61];
sesList = [01 02 03 04];

subj = 01
ses = 04

% loop over subjects + timepoints
for subj = subj %subjList
    for j = ses %1:numel(sesList) %ses


    subj = num2str(subj, '%02d'); % zero-pads variables to be 2 characters long
    ses = num2str(j, '%02d');


    fprintf('subj-%s session-%s', subj, ses)


%% check if session is missing, then skip
if exist([dataDir 'sub-' subj '/ses-' ses '/func/']) == 0
    continue 
end
   


%% define nii input (smoothed FWHM 6mm fmriprep output)
input_nii=([dataDir 'sub-' subj '/ses-' ses '/func/ssub-' subj '_ses-' ses '_task-wanting_space-MNI152NLin2009cAsym_desc-preproc_bold.nii']);


%% create vectors for onsets
% target file: /data/p_02020/MRI_data/BIDS/sub-01/ses-01/func/sub-01_ses-01_task-wanting_events.tsv
logfile_tmp = tsv_read([dataDir_behav 'sub-' subj '/ses-' ses '/func/sub-' subj '_ses-' ses '_task-wanting_events.tsv'], '',true);
vars = fieldnames(logfile_tmp);
vars_new = erase(vars,"x0x22");
vars_new = erase(vars_new,"0x22");
logfile = cell2struct(struct2cell(logfile_tmp), vars_new);


%% make cell arrays for specific onsets (onset arrays and reward rating arrays)
onsets_pictures = [];
onsets_buttonpress = [];
reward_pictures = [];


for i = 1:size(logfile.stim_type,1)
    if strcmp(logfile.trial_type{i,1}(2:end-1),'Response')
        onsets_buttonpress(end+1) = logfile.onset(i);
    end
    if strcmp(logfile.trial_type{i,1}(2:end-1),'Picture')
        onsets_pictures(end+1) = logfile.onset(i);
        reward_pictures(end+1) =  str2num(string(logfile.wanting(i)));
    end
end




%% load motion params (6 rigid-body motion parameters and the binary TR outlier regressor (>0.9mm))
tParams.motion_regress ='yes'; % use  motion  parameters  as  regressors? yes/no

% 6 rigid-body motion params - fmriprep output
% target file: /data/pt_02020/MRI_data/fmriprep/fmriprep/sub-01/ses-01/func/sub-01_ses-01_task-wanting_desc-confounds_regressors.tsv
motion_reg_tmp = tsv_read([dataDir 'sub-' subj '/ses-' ses '/func/sub-' subj '_ses-' ses '_task-wanting_desc-confounds_regressors.tsv'],'',true);

% select columns of interest (trans x/y/z and rot x/y/z)
motion_reg=selectfields(motion_reg_tmp,{'trans_x', 'trans_y', 'trans_z','rot_x','rot_y','rot_z'}) 

% % load TR outlier regressor
% % target file for binary regressor: /data/pt_02020/MRI_data/fmriprep/fmriprep/sub-01/ses-03/func/sub-01_ses-03_task-wanting_movement-confounds.tsv

% % note some subjects don't have files if all are 0
% if session is missing, then skip
if exist([dataDir 'sub-' subj '/ses-' ses '/func/sub-' subj '_ses-' ses '_task-wanting_movement-confounds.tsv'])
    % xx = [dataDir 'sub-' subj '/ses-' ses '/func/sub-' subj '_ses-' ses '_task-wanting_movement-confounds.tsv'];
   % load(xx)
   motion_reg_binary = tsv_read([dataDir 'sub-' subj '/ses-' ses '/func/sub-' subj '_ses-' ses '_task-wanting_movement-confounds.tsv'],'', false)
   
   % x = ([dataDir 'sub-' subj '/ses-' ses '/func/sub-' subj '_ses-' ses '_task-wanting_movement-confounds.tsv'])
   % motion_reg_binary = load(xx);
   % % to do: check format of input -> why 950 lines but multiple integers per cell?       
   % select 1 column only
   % x =motion_reg_binary(:,1)
   % motion_reg_binary.Var1{:,1}(:,1)
   % append binary to motion param 
   %motion_reg.Var1  = motion_reg_binary.Var1
   
end


% make subject-session-specific outdir
mkdir([outdir 'sub-' subj '/ses-' ses]);
wd = ([outdir 'sub-' subj '/ses-' ses]);

%% change to output dir per sub per ses
cd([outdir 'sub-' subj '/ses-' ses]);

% % to do: load into matlab as regressor
writetable(struct2table(motion_reg), 'motion_reg.txt','WriteVariableNames',false)


%%% define dir and load input files
matlabbatch{1}.spm.stats.fmri_spec.dir = cellstr(wd);
matlabbatch{1}.spm.stats.fmri_spec.timing.units = 'secs';
matlabbatch{1}.spm.stats.fmri_spec.timing.RT = 2; %% interscan interval (TR)
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t = 16; % microtime resolution or number of slices
matlabbatch{1}.spm.stats.fmri_spec.timing.fmri_t0 = 8; % microtime onset
matlabbatch{1}.spm.stats.fmri_spec.sess(1).scans = cellstr(input_nii);

%% load onsets of picture stimuli

%% sanity check only (all picture onsets + buttonpress)
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).name = 'pictures';
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).onset = [onsets_pictures];
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).duration = 4;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).tmod = 0;
%matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod = struct('name', {}, 'param', {}, 'poly', {});

matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod.name = 'wanting value';
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod.param = [reward_pictures];
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).pmod.poly = 1;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(1).orth = 1;


%% load onsets button press
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).name = 'button press';
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).onset = [onsets_buttonpress];
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).duration = 0;
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).tmod = 0;

matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).pmod = struct('name', {}, 'param', {}, 'poly', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(1).cond(2).orth = 1;


%%
matlabbatch{1}.spm.stats.fmri_spec.sess(1).multi = {''};
matlabbatch{1}.spm.stats.fmri_spec.sess(1).regress = struct('name', {}, 'val', {});
matlabbatch{1}.spm.stats.fmri_spec.sess(1).multi_reg = {'motion_reg.txt'}; %% 6 motion params
matlabbatch{1}.spm.stats.fmri_spec.sess(1).hpf = 128;



%% Model params
matlabbatch{1}.spm.stats.fmri_spec.fact = struct('name', {}, 'levels', {});
matlabbatch{1}.spm.stats.fmri_spec.bases.hrf.derivs = [0 0];
matlabbatch{1}.spm.stats.fmri_spec.volt = 1;
matlabbatch{1}.spm.stats.fmri_spec.global = 'None';
matlabbatch{1}.spm.stats.fmri_spec.mthresh = 0.8;
matlabbatch{1}.spm.stats.fmri_spec.mask = {''}; %% to do: define mask?
matlabbatch{1}.spm.stats.fmri_spec.cvi = 'AR(1)';



%% Model specification
matlabbatch{2}.spm.stats.fmri_est.spmmat(1) = cfg_dep('fMRI model specification: SPM.mat File', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{2}.spm.stats.fmri_est.write_residuals = 0;
matlabbatch{2}.spm.stats.fmri_est.method.Classical = 1;

%% Model estimation and setting of contrasts
%%% note: the onsets order: 
% % % onsets_pictures
% % % onsets_buttonpress
% % % reward_pictures
% % % last column: average
matlabbatch{3}.spm.stats.con.spmmat(1) = cfg_dep('Model estimation: SPM.mat File', substruct('.','val', '{}',{2}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','spmmat'));
matlabbatch{3}.spm.stats.con.consess{1}.tcon.name = 'pictures > buttonpress';
matlabbatch{3}.spm.stats.con.consess{1}.tcon.weights = [1 -1 0];
matlabbatch{3}.spm.stats.con.consess{1}.tcon.sessrep = 'repl';
matlabbatch{3}.spm.stats.con.consess{2}.tcon.name = 'buttonpress > pictures';
matlabbatch{3}.spm.stats.con.consess{2}.tcon.weights = [-1 1 0];
matlabbatch{3}.spm.stats.con.consess{2}.tcon.sessrep = 'repl';
matlabbatch{3}.spm.stats.con.consess{3}.tcon.name = 'wanting modulation for pictures';
matlabbatch{3}.spm.stats.con.consess{3}.tcon.weights = [0 0 1];
matlabbatch{3}.spm.stats.con.consess{3}.tcon.sessrep = 'repl';
matlabbatch{3}.spm.stats.con.delete = 0;

%% Run matlabbatch jobs
spm_jobman('run', matlabbatch);

   end
end